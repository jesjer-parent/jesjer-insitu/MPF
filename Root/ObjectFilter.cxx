#include "InSituJES/ObjectFilter.h"
#include "xAODEventInfo/EventInfo.h"
#include <vector>
#include "xAODEgamma/EgammaDefs.h"

// Tools
//#include "JetSelectorTools/JetCleaningTool.h"
#include "CPAnalysisExamples/errorcheck.h"
#include "ElectronPhotonSelectorTools/AsgPhotonIsEMSelector.h"

// Egamma EDM
#include "xAODEgamma/PhotonxAODHelpers.h"

#define CHECK( ARG )                                  \
do {                                                  \
    const bool result = ARG;                          \
  if( ! result ) {                                    \
      ::Error( APP_NAME, "Failed to execute: \"%s\"", \
#ARG );                                               \
      return 1;                                       \
  }                                                   \
 } while( false )



using std::cout;
using std::endl;

ObjectFilter::ObjectFilter()
{
}

ObjectFilter::~ObjectFilter()
{
}

bool ObjectFilter::Init(TString Config,  std::vector<std::string> *cuts, bool ismc)
{
  const char* APP_NAME = "Init";
  isMC = ismc;
  TEnv * settings = new TEnv();
  int status = settings->ReadFile(Config,EEnvLevel(0));
  if (status!=0) {
     cout<<"cannot read config file in ObjectFilter"<< endl;
     cout << Config << endl;
     cout<<"******************************************\n"<< endl;
     return 1;
  }

  Mode      		= settings->GetValue("Mode", "None");

  // ///////////////
  // Get Photon cuts
  // ///////////////
  MinPt_Photon          = settings->GetValue("leadingPhoton_minPt", 0);
  MaxEta_Photon		= settings->GetValue("leadingPhoton_maxEta", 1.37);
  PhQual 		= settings->GetValue("leadingPhoton_ID", "Tight");

  // /////////////////
  // Get Electron cuts
  // /////////////////
  ElQual		= settings->GetValue("Eletron_ID", "medium");
  MinPt_Electron	= settings->GetValue("Electron_min_Pt", 0);


  // /////////////
  // Get Muon cuts
  // /////////////
  MinPt_Muon		= settings->GetValue("MinPt_Muon", 0);
  MaxEta_Muon		= settings->GetValue("MaxEta_Muon", 999.);
  RelIsolation_Muon	= settings->GetValue("RelIsolation_Muon", 0.1);
  delete settings;
  

  if (Mode == "Muon") {
    m_MuonSelectorTool = new CP::MuonSelectionTool("MuonSelection");
    //m_MuonSelectorTool->msg().setLevel( MSG::INFO );
    m_MuonSelectorTool->setProperty( "MaxEta", MaxEta_Muon ).ignore();
    m_MuonSelectorTool->setProperty( "MuQuality", 3).ignore(); // Tight, Medium, Loose, VeryLoose}, corresponding to 0, 1, 2 and 3, default is 3
    if (! m_MuonSelectorTool->initialize().isSuccess() ){
      Error("Init()", "Failed to properly initialize the MuonSelector Tool. Exiting." );
      return false;
    }
  }

  
  // /////////////////////
  // Isolation tool
  // ///////////////////// 
  m_EgammaIsolationSelectionTool = new CP::IsolationSelectionTool("IsolationTool");
  // //////////////////
  // Set isolation cuts
  // //////////////////

  isoCorr_tool = new CP::IsolationCorrectionTool("isoCorr_tool");
  //isoCorr_tool->setProperty( "IsMC", isMC); //if MC, else false
  if (! isoCorr_tool->initialize().isSuccess() ){
    Error("initialize()", "Failed to properly initialize the isoCorr_tool. Exiting." );
    return false;
  }

  CHECK( m_EgammaIsolationSelectionTool->setProperty("MuonWP","Loose") );
  CHECK( m_EgammaIsolationSelectionTool->setProperty("ElectronWP","Loose") );
  CHECK( m_EgammaIsolationSelectionTool->setProperty("PhotonWP", "FixedCutTight") ); //Cone20") );
  if (! m_EgammaIsolationSelectionTool->initialize().isSuccess()) {
    Error("initialize()", "Failed to properly initialize EgammaIsolationSelectionTool. Exiting." );
    return false;
  }


  // /////////////////
  // Photon Fudge Tool
  // ///////////////// 
  if (Mode == "Photon"){
    m_fudgeMCTool = new ElectronPhotonShowerShapeFudgeTool ( "FudgeMCTool" );
    int FFset = 16; // for MC15 samples, which are based on a geometry derived from GEO-21
    m_fudgeMCTool->setProperty("Preselection", FFset);
    if (!m_fudgeMCTool->initialize().isSuccess()) {
      Fatal("MyFunction", "Failed to initialize FudgeMCTool ");
    }

    // ////////////////
    // Photon IsEM Tool
    // ////////////////
    m_photonTightIsEMSelector = new AsgPhotonIsEMSelector ( "PhotonTightIsEMSelector" );
    m_photonTightIsEMSelector->setProperty("isEMMask",egammaPID::PhotonTight).ignore();
    m_photonTightIsEMSelector->setProperty("ConfigFile","ElectronPhotonSelectorTools/offline/mc15_20150712/PhotonIsEMTightSelectorCutDefs.conf").ignore();
    if (!m_photonTightIsEMSelector->initialize().isSuccess()) {
      Error("initialize()", "Failed to properly initialize PhotonTightIsEMSelector. Exiting." );
      return false;
    }

    m_photonLooseIsEMSelector = new AsgPhotonIsEMSelector ( "PhotonLooseIsEMSelector" );
    m_photonLooseIsEMSelector->setProperty("isEMMask",egammaPID::PhotonLoose).ignore();
    m_photonLooseIsEMSelector->setProperty("ConfigFile","ElectronPhotonSelectorTools/offline/mc15_20150712/PhotonIsEMLooseSelectorCutDefs.conf").ignore();
    if (!m_photonLooseIsEMSelector->initialize().isSuccess()) {
      Error("initialize()", "Failed to properly initialize PhotonLooseIsEMSelector. Exiting." );
      return false;
    }

  const unsigned int LOOSEPRIME =
      0x1 << egammaPID::ClusterStripsWtot_Photon|
      0x1 << egammaPID::ClusterStripsDEmaxs1_Photon;
   const unsigned int PhotonLoosePrime = egammaPID::HADLEAKETA_PHOTON | egammaPID::CALOMIDDLE_PHOTON | LOOSEPRIME;
  

    m_photonLoosePIsEMSelector = new AsgPhotonIsEMSelector ( "PhotonLoosePIsEMSelector" );
    m_photonLoosePIsEMSelector->setProperty("isEMMask",PhotonLoosePrime).ignore();
    m_photonLoosePIsEMSelector->setProperty("ConfigFile","ElectronPhotonSelectorTools/offline/mc15_20150712/PhotonIsEMTightSelectorCutDefs.conf").ignore();
    if (!m_photonLoosePIsEMSelector->initialize().isSuccess()) {
      Error("initialize()", "Failed to properly initialize PhotonLoosePIsEMSelector. Exiting." );
      return false;
    }

  }

  std::string confDir = "ElectronPhotonSelectorTools/offline/mc15_20160512/";
//ElectronPhotonSelectorTools/offline/mc15_20150429/";
  m_LHTool = new AsgElectronLikelihoodTool("m_LHToolLoose"); // create the selector
//  m_LHTool->setProperty("primaryVertexContainer","PrimaryVertices").ignore();
  m_LHTool->setProperty("WorkingPoint", "LooseLHElectron").ignore();
  //m_LHTool->setProperty("ConfigFile",confDir+"ElectronLikelihoodLooseOfflineConfig2016_Smooth.conf").ignore();
//ElectronLikelihoodLooseOfflineConfig2015.conf").ignore();
  if (!m_LHTool->initialize().isSuccess()) {
      Error("initialize()", "Failed to properly initialize IsEMSelector. Exiting." );
      return false;
  }
     
  Cuts = cuts;
  return true;
}


bool ObjectFilter::PassJet(const xAOD::Jet *jet)
{
  cout << jet->p4().Pt() << endl;
  return true;
}


bool ObjectFilter::PassPhoton(xAOD::Photon *photon, std::vector<int> *failed, bool isNom)
{
  // ///////////////////////////////
  // Don't return false right away  
  // if the photon fails the nominal
  // selection.  Photon may still be 
  // useful in measuring the purity. 
  // ///////////////////////////////
  int pass = true;
  FirstFailed = 0;

  PassNomID   = false;
  PassNomIsol = false;
  if (isMC){
    if (m_fudgeMCTool->applyCorrection(*photon) == CP::CorrectionCode::Error){
        Error("PassPhoton()", "Failed to apply photon fudge factors. Exiting." );
        return false;
    }
  }


  // /////////////
  // Photon Author
  // /////////////
  uint16_t author =  photon->author();
  if (not ((author & xAOD::EgammaParameters::AuthorPhoton) or (author & xAOD::EgammaParameters::AuthorAmbiguous))){
    failed->push_back(FindCode("Photon: author"));
    return false;
  }

  // ///////////
  // Photon isEM
  // ///////////

  // The egamma tool seems to write over the initial stored values so each time you fudge
  // the corrections get compounded.  The solution is to just make a new copy and send the copy into
  // the tools, but for now I'm just going to keep the first ID decision and use that for all
  // systematics.  
  if (isNom){
    isTight = -1;
    bool isLoose = m_photonLooseIsEMSelector->accept(photon);
    if(! photon->passSelection(isLoose, "Loose")) {
      std::cout<<"WARNING: Loose decision is not available" <<std::endl;
    }
  
    bool isLooseP = m_photonLoosePIsEMSelector->accept(photon);
    bool tight = m_photonTightIsEMSelector->accept(photon);
    isLoose = isLooseP; 
   
    //cout << "loose " << isLoose << ", tight " << tight << endl;
    if (isLoose) isTight = 0;
    if (not isLoose){
      failed->push_back(FindCode("Photon: tight"));
      return false;
    }

    if (tight) isTight = 1;
    else if (isLoose){
     failed->push_back(FindCode("Photon: tight"));
    }
    if (isTight == 1) PassNomID = true;
  }

  // /////////////
  // Photon Pt cut
  // ///////////// 
  if ((photon)->pt()/GeV < MinPt_Photon){
    failed->push_back(FindCode("Photon: Pt"));
    return false;
  }
  

  // //////////
  // Photon Eta
  // //////////
  if (fabs(photon->eta()) > MaxEta_Photon){
    failed->push_back(FindCode("Photon: Eta"));
    return false;
  }

  // ////////////
  // Photon crack
  // ////////////
  if (fabs(photon->eta()) > 1.37 and fabs(photon->eta()) < 1.52){
    failed->push_back(FindCode("Photon: crack"));
    return false;
  }

  // /////////
  // Isolation
  // /////////
//  if (isoCorr_tool->CorrectLeakage((*photon)) == CP::CorrectionCode::Error){
  if (isoCorr_tool->applyCorrection((*photon)) == CP::CorrectionCode::Error){
    Error("PassPhoton()", "Failed to apply photon fudge factors. Exiting." );
    return false;
  }
  static SG::AuxElement::Accessor<float> Isol( "topoetcone40" );
  IsolE = Isol(*photon); 

  isIsol=0;
  if (!m_EgammaIsolationSelectionTool->accept( *photon )){
    isIsol=1;
    failed->push_back(FindCode("Photon: Isolation"));
  }
  else PassNomIsol = true;

 
  bool passEoverP = true;
  // E/p cut for converted photons
  if (xAOD::EgammaHelpers::conversionType(photon) > 0){
    double clusterEt = photon->caloCluster()->e() / cosh(photon->caloCluster()->eta());
    double trackPt  = xAOD::EgammaHelpers::momentumAtVertex(photon).perp();
    if (xAOD::EgammaHelpers::conversionType(photon) > 2){
      if (clusterEt / trackPt > 1.5 or clusterEt / trackPt < 0.5) passEoverP = false;
    }
    else if (clusterEt / trackPt > 2.0) passEoverP = false;
  }

  if (not passEoverP){ 
    failed->push_back(FindCode("Photon: Conversion"));
    return false;
   }

  return pass;
}

////////////////////////////////////////////////////////
bool ObjectFilter::PassElectron(xAOD::Electron *electron)
{
  FirstFailed = 0;

  // ///////////
  // Electron ID
  // ///////////
  bool isloose=m_LHTool->accept((*electron));
  if (!isloose){
     //cout << "fail loose" << endl;
    return false;
  }


  // ///////////////
  // Electron Pt cut
  // ///////////////
  if ((electron)->pt()/GeV < MinPt_Electron){
    return false;
  }

  //if( ! electron->isGoodOQ(xAOD::EgammaParameters::BADCLUSELECTRON) ) return false

  // ////////////////
  // Electron Eta cut
  // ////////////////
  const xAOD::CaloCluster* eCluster(0);
  if ( (electron)->nCaloClusters() > 0) {eCluster = (electron)->caloCluster(0);}
  else {return false;}
  if ( fabs(eCluster->eta()) > 2.47) return false;
  if ( (fabs(eCluster->eta()) > 1.37) and (fabs(eCluster->eta()) > 1.52)) return false;
 

  
  // /////////
  // Isolation
  // /////////
//  if (isoCorr_tool->CorrectLeakage((*photon)) == CP::CorrectionCode::Error){
  if (isoCorr_tool->applyCorrection((*electron)) == CP::CorrectionCode::Error){
    Error("PassPhoton()", "Failed to apply electron isolation correction. Exiting." );
    return false;
  }

  bool isPass = m_EgammaIsolationSelectionTool->accept( *electron );
  //cout << "isol " << isPass << endl;
  //cout << isPass << endl;
  //isPass = true;  
  return isPass;
}

bool ObjectFilter::PassMuon(const xAOD::Muon *muon)
{

  // ///////
  // Quality
  // ///////
  if(!m_MuonSelectorTool->accept(muon)) return false;
  if(m_MuonSelectorTool->isBadMuon(*muon))return false;

  // //////
  // Author
  // //////
 // if (!(muon)->isAuthor(xAOD::Muon::Author)) return false;

  // ///////////
  // Muon Pt cut
  // ///////////
  if ((muon)->pt()/GeV < MinPt_Muon){
    //cout << " Pt " << (muon)->pt()/GeV << endl;
    return false;
  }

  // ////////
  // Muon eta
  // ////////
  if ( fabs(muon->eta()) > MaxEta_Muon){
    //cout << fabs(muon->eta()) << " eta" << endl;
    return false;
  }
   

  // /////////
  // Isolation
  // /////////
  bool isPass = m_EgammaIsolationSelectionTool->accept( *muon );
  if (not isPass) return false;
//  if ( muon->auxdata<float>("ptcone20") > 1.8 * GeV){
    //cout << muon->auxdata<float>("ptcone20")/GeV << " Isol " << endl;
//    return false;
//  }

//  if ( muon->auxdata<float>("ptcone20") / muon->pt() > RelIsolation_Muon){
//    return false;
//  }


  // //////////////////
  // Track requirements
  // Muon correction for MET done using these cuts
  // 3 pixel and 5 silicon hits, and 3 precision MS hits  
/*  uint8_t PixHits = 0, SCTHits = 0, nprecisionLayers = 0;
  if(!(muon)->primaryTrackParticle()->summaryValue(PixHits,xAOD::numberOfPixelHits))
    cout << "no pixel information" << endl;
  if(!(muon)->primaryTrackParticle()->summaryValue(SCTHits,xAOD::numberOfSCTHits))
    cout << "no silicon information!" << endl;
  if (!(muon)->primaryTrackParticle()->summaryValue(nprecisionLayers, xAOD::numberOfPrecisionLayers))
    std::cout << "no nprecisionlayers! " << std::endl;
*///  if ( (PixHits < 3) or (SCTHits < 5) or (nprecisionLayers < 3) ) return false;

  return true;
}

void ObjectFilter::Clean()
{
  if (Mode == "Photon") {
    delete m_photonTightIsEMSelector;
    delete m_photonLooseIsEMSelector;
  }
  delete isoCorr_tool;
  if (Mode == "Muon" and m_MuonSelectorTool) delete m_MuonSelectorTool;
  if (! m_EgammaIsolationSelectionTool->finalize().isSuccess() )Error("Clean()", "Failed to properly finalize the EgammaIsolationSelectionTool. Exiting." );
  delete m_EgammaIsolationSelectionTool;
  if (!m_LHTool->finalize().isSuccess()) Error("Clean()", "Failed to properly finalize egammaID tool. Exiting." );
  delete m_LHTool;
}

bool ObjectFilter::LoosePrime(unsigned int val)
{
  unsigned int mask = 1 << (sizeof(int) * 8 - 1);
  for(unsigned int i = 0; i < sizeof(int) * 8; i++){
    if (i == 10 or i==11 or i==12 or i==13 or i==14 or i==18 or i==21){
      if( (val & mask) == 1 ) return false;
     }
     mask  >>= 1;
  }
  return true;
}
