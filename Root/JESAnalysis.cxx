#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <InSituJES/JESAnalysis.h>
#include <InSituJES/EventFilter.h>
#include <EventLoop/OutputStream.h> // needed when running on the grid
#include <math.h>
#include <string>
#include <vector>
#include <iostream>
#include <sstream>
#include <cstdlib>

// EDM includes:
#include "CPAnalysisExamples/errorcheck.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODCaloEvent/CaloCluster.h"
#include "xAODPFlow/PFOContainer.h"
#include "xAODTracking/TrackParticle.h"


#include "BootstrapGenerator/TH1DBootstrap.h"
// GRL
#include "GoodRunsLists/GoodRunsListSelectionTool.h"

using namespace std;
using namespace Trig;
using namespace TrigConf;
using namespace xAOD;

// this is needed to distribute the algorithm to the workers
ClassImp(JESAnalysis)


JESAnalysis::JESAnalysis ()
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
  

}

JESAnalysis::~JESAnalysis ()
{
  map<string,TH2DBootstrap*>::iterator h4Itr =  m_data.begin();
  map<string,TH2DBootstrap*>::iterator h4ItrE = m_data.end();
  for(; h4Itr != h4ItrE; ++h4Itr){
    delete (*h4Itr).second;
  }
}



EL::StatusCode JESAnalysis::setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.

  job.useXAOD ();

  // let's initialize the algorithm to use the xAODRootAccess package
  xAOD::Init( "JESAnalysis" ).ignore(); // call before opening first file

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode JESAnalysis::histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  SetConfig();
  theFilter = new EventFilter(TString(Config));  
  //ShoweringTool=new Showering();
  m_TH1["MPF_CutInfo"] = new TH1D("hMPFCutInfo", "hMPFCutInfo", 40, 0, 40);
  m_TH1["Bal_CutInfo"] = new TH1D("hBalCutInfo", "hBalCutInfo", 40, 0, 40);

  m_TH1["Isolation"]   = new TH1D("hIsolation", "hIsolation", 240, -20, 100);
  m_TH1["Weights"]     = new TH1D("hWeights", "hWeights", 1000, 0, 50);
  m_TH1["SumWeights"]  = new TH1D("hSumWeights", "hSumWeights", 500, 361000, 361500);

  // //////////////////////////////////////////
  // Attempting to further automate the 
  // my plotting scripts by making the output
  // keep track of the run it was made on
  // 
  // The reason I'm awkwardly keeping it in 
  // histogram form is that it's acceptable to 
  // combine electrons and muons to get Z+jet 
  // and I figured why not allow for other 
  // possibilities even though I doubt they'll
  // ever make much sense.   
  // //////////////////////////////////////////
  

  m_TH1["Channel"]     = new TH1D("hChannel", "hChannel", 3, 0, 3); // Electron, muon, or photon
  m_TH1["Collection"]  = new TH1D("hCollection", "hCollection", 3, 0, 3); // For now just assuming AntiKt4, so EM, LC and PFlow
  m_TH1["Sample"]      = new TH1D("hSample", "hSample", 4,0,4); // Data, nominal MC and alternative MC
  
  int channel=999;
  if (theFilter->mode == "Electron")    channel=0;
  else if (theFilter->mode == "Muon")   channel=1;
  else if (theFilter->mode == "Photon") channel=2;
  m_TH1["Channel"]->Fill(channel);
 
  int collection=999;
  if (theFilter->name=="AntiKt4EMTopo")      collection=0;
  else if (theFilter->name=="AntiKt4LCTopo") collection=1;
  else if (theFilter->name=="AntiKt4EMPFlow")collection=2;
  m_TH1["Collection"]->Fill(collection);

  int sample=999;
  if (theFilter->isMC!=true)     sample=0;
  else{
    if (TString(wk()->inputFile()->GetName()).Contains("Pythia")) sample=1;
    else if (TString(wk()->inputFile()->GetName()).Contains("Sherpa")) sample=2;
    else sample=3;
  }
  m_TH1["Sample"]->Fill(sample);


  // ///////////////////////////////////////////
  // I'm keeping a bin which goes to 10 TeV just
  // so it's easier to see when the bins in the 
  // high Pt region need to be reconsidered 
  // ///////////////////////////////////////////
  double gProbePtBins[]={25., 45., 65., 85., 105., 125., 160., 210., 260., 310., 400., 500., 600., 800., 1000, 1200, 1400, 1600, 2000, 10000.};
  n_gProbePtBins=sizeof(gProbePtBins)/sizeof(double)-1;

  double zProbePtBins[]={17., 20., 25., 30., 35., 45., 60., 80., 110., 160., 210.,   260., 350., 800, 1000, 1200, 10000};
  UInt_t n_zProbePtBins=sizeof(zProbePtBins)/sizeof(double)-1;
  
  if (theFilter->GetMode() == "Photon") {
    for (unsigned int i = 0; i <= n_gProbePtBins;i++) {ptBins.push_back( gProbePtBins[i] );}
  }   else {
    for (unsigned int i = 0; i <= n_zProbePtBins;i++){
      ptBins.push_back( zProbePtBins[i] );
    }
  } 
  n_ptBins =  ptBins.size()-1;


  m_TH2["jApprox"]       = new TH2D("hjApprox",                  "hjApprox",                  n_ptBins, (&(*(ptBins.begin()))), 700, -3,4);
  m_TH2["Response"]      = new TH2D("hResponseProbePT",          "hResponseProbePT",          n_ptBins, (&(*(ptBins.begin()))), 700, -3,4);
  m_TH2["ResponseNoGSC"] = new TH2D("hResponseNoGSCProbePT",     "hResponseNoGSCProbePT",          n_ptBins, (&(*(ptBins.begin()))), 700, -3,4);
  m_TH2["ResponseHigh"]  = new TH2D("hResponseProbePTHigh",      "hResponseProbePTHigh",      n_ptBins, (&(*(ptBins.begin()))), 700, -3,4);
  m_TH2["ResponseLow"]   = new TH2D("hResponseProbePTLow",       "hResponseProbePTLow",       n_ptBins, (&(*(ptBins.begin()))), 700, -3,4);
  m_TH2["Balance"]       = new TH2D("hBalanceProbePT",           "hBalanceProbePT",           n_ptBins, (&(*(ptBins.begin()))), 700, -3,4);
  m_TH2["RawBalance"]    = new TH2D("hRawBalanceProbePT",        "hRawBalanceProbePT",        n_ptBins, (&(*(ptBins.begin()))), 700, -3,4);
  m_TH2["Mapping"]       = new TH2D("hMappingProbePT",           "hMappingProbePT",           n_ptBins, (&(*(ptBins.begin()))), 3000, 0,1500);
  m_TH2["Runs"]          = new TH2D("hResponseRuns",             "hResponseRuns",             70000,    260000, 320000, 700, -3, 4);
  m_TH2["Runs35"]        = new TH2D("hResponseRuns35",           "hResponseRuns35",             70000,    260000, 320000, 700, -3, 4);



// These plots aren't strictly needed since the same information is stored using
// the systematic tool.  It just seems like a good idea to double check from 
// every now and then that the syst tool is doing what it should be doing. 
 
  m_TH2["J2up"]       = new TH2D("hResponseProbePTJ2up",      "hResponseProbePTJ2up",      n_ptBins, (&(*(ptBins.begin()))), 700, -3,4);
  m_TH2["J2down"]     = new TH2D("hResponseProbePTJ2down",    "hResponseProbePTJ2down",    n_ptBins, (&(*(ptBins.begin()))), 700, -3,4);
  m_TH2["dPhiup"]     = new TH2D("hResponseProbePTdPhiup",    "hResponseProbePTdPhiup",    n_ptBins, (&(*(ptBins.begin()))), 700, -3,4);
  m_TH2["dPhidown"]   = new TH2D("hResponseProbePTdPhidown",  "hResponseProbePTdPhidown",  n_ptBins, (&(*(ptBins.begin()))), 700, -3,4);


  int coneSizes[]={1,2,3,4,5,6,7,8,9,10}; //!
  UInt_t n_coneSizes=sizeof(coneSizes)/sizeof(int); //!
  for (int size=0; size<n_coneSizes; size++){
    string ShowerString="OtherActivity"+to_string(coneSizes[size]);
    string ShowerHistString="hOtherActivity"+to_string(coneSizes[size])+"VsPt";
    m_TH2[ShowerString] = new TH2D(ShowerHistString.c_str(), ShowerHistString.c_str(), n_ptBins, (&(*(ptBins.begin()))),  200, -1, 3);
  }



  m_TH2["AppliedJES"]   = new TH2D("hAppliedJES",             "hAppliedJES",               n_ptBins, (&(*(ptBins.begin()))), 200, 0,2);
  m_TH2["AppliedGSC"]   = new TH2D("hAppliedGSC",             "hAppliedGSC",               n_ptBins, (&(*(ptBins.begin()))), 200, 0,2);
  m_TH2["TrueResponse"] = new TH2D("hTrueResponseProbePT",    "hTrueResponseProbePT",      n_ptBins, (&(*(ptBins.begin()))), 200, 0,2);
  m_TH2["MPFTrueRatio"] = new TH2D("hTrueMPFRatio",           "hTrueMPFRatio",             n_ptBins, (&(*(ptBins.begin()))), 700, -2,5);
  m_TH2["BalTrueRatio"] = new TH2D("hTrueBalRatio",           "hTrueBalRatio",             n_ptBins, (&(*(ptBins.begin()))), 700, -2,5);
  m_TH2["TrueMF"]       = new TH2D("hTrueMFProbePT",          "hTrueMFProbePT",            n_ptBins, (&(*(ptBins.begin()))), 100, 0,1);
  
  m_TH1["MPF"]   = new TH1D("hMPF",  "hMPF",  700, -3,4);

  vector<double>::const_iterator it;
  for (it = ptBins.begin(); it != ptBins.end()-1; it++) {
    string name  = "ResponseVsRad_"    + to_string((int)*it) + "_to_";
    string name2 = "ResponseVsPileup_" + to_string((int)*it) + "_to_";
    string name3 = "ResponseVsJetEta_" + to_string((int)*it) + "_to_";
    string name4 = "ResponseVsJetDistances_" + to_string((int)*it) + "_to_";
    it++;
    name  += to_string((int)*it) + "_GeV";
    name2 += to_string((int)*it) + "_GeV";
    name3 += to_string((int)*it) + "_GeV";
    name4 += to_string((int)*it) + "_GeV";
    it--;
    if (doRadiationStudies)   m_TH3[name.c_str()]    = new TH3D(name.c_str(),  name.c_str(),  350,-3,4,10,0,0.5,11,2.6,3.15);
    if (doPileupStudies)      m_TH3[name2.c_str()]   = new TH3D(name2.c_str(), name2.c_str(), 350,-3,4,20,0,60,41,-0.5,40.5); //50, 120
    if (doJetEtaStudies)      m_TH3[name3.c_str()]   = new TH3D(name3.c_str(), name3.c_str(), 350,-3,4,20,-5,5,22,-6,5); // 100, 110
    if (doJetDistanceStudies) {
      m_TH3[name4.c_str()]   = new TH3D(name4.c_str(), name4.c_str(), 350,-3,4,20,0,5,20,0,5);
      m_TH3[name4.c_str()]->GetXaxis()->SetTitle("#DeltaR(leadingJet, nearestjet)");
      m_TH3[name4.c_str()]->GetXaxis()->SetTitle("#DeltaR(leadingJet, subleading)");
    }
    if (doRingStudies){
      name  = "EnergyRing_"        + to_string((int)*it) + "_to_";
      it++;
      name  += to_string((int)*it) + "_GeV";
      it--;
      m_TH2[name.c_str()]   = new TH2D(name.c_str(),  name.c_str(),  numRings,0,furthestRing, 200, 0,1);
    }
   
  }


  // Jet kinematic plots
  m_TH1["JetPt"]  =new TH1D("hJetPT", "hJetPT", 200, 0, 1000);
  m_TH1["JetEta"] =new TH1D("hJetEta", "hJetEta", 100, -5, 5);
 
  m_TH1["METx"] =new TH1D("hMETx", "hMETx", 400, -200, 200);
  m_TH1["METy"] =new TH1D("hMETy", "hMETy", 400, -200, 200);

  m_TH2["MuVsRho"]            =new TH2D("hMuVsRho", "hMuVsRho", 300, 0, 75, 200, 0, 50);
  m_TH2["ClusOverEl"]         =new TH2D("hClusOverEl", "hClusOverEl", 100, 20, 1000,  200, 0, 2);
  m_TH2["ClusOverUncalEl"]    =new TH2D("hClusOverUncalEl", "hClusOverUncalEl", 100, 20, 1000, 200, 0, 2);
  m_TH2["CalClusOverEl"]      =new TH2D("hCalClusOverEl", "hCalClusOverEl", 100, 20, 1000, 200, 0, 2);
  m_TH2["CalClusOverUncalEl"] =new TH2D("hCalClusOverUncalEl", "hCalClusOverUncalEl", 100, 20, 1000, 200, 0, 2);
  m_TH2["CalElOverUncalEl"]   =new TH2D("hCalElOverUncalEl", "hCalElOverUncalEl",  600, -3, 3, 200, 0, 2);


  // Reference kinematic plots
  m_TH1["LeptonPt"]=new TH1D("hLeptonPt", "hLeptonPt", 200,0,1000);
  
  m_TH1["dPhiLep1Jet"]=new TH1D("hdPhiLep1Jet", "hdPhiLep1Jet", 75, 0, 3.15);
  m_TH1["dPhiLep2Jet"]=new TH1D("hdPhiLep2Jet", "hdPhiLep2Jet", 75, 0, 3.15);

  m_TH1["RefPt"]=new TH1D("hRefPT", "hRefPT", 900, 0, 9000);
  m_TH1["RefEta"]=new TH1D("hRefEta", "hRefEta", 100, -5, 5);
  m_TH1["RefMass"]=new TH1D("hRefMass", "hRefMass", 100, 60, 120);

  m_TH1["AvInt"]=new TH1D("hAvInt", "hAvInt", 41, -0.5, 40.5);
  m_TH1["NPV"]=new TH1D("hNPV", "hNPV", 51, -0.5, 50.5);


  // Plots other activity studies (aka. Rogayeh plots)
  m_TH1["OA"]=new TH1D("hOtherActivity", "hOtherActivity", 200, -1., 3.);
  m_TH2["OAVsPt"]=new TH2D("hOtherActivityProbePT", "hOtherActivityProbePT", n_ptBins, (&(*(ptBins.begin()))), 20, -1., 3.);
  m_TH2["OAClose"]=new TH2D("OACloseProbePT", "OACloseProbePT", n_ptBins, (&(*(ptBins.begin()))), 1000, -100, 300);
  m_TH2["OACloseRel"]=new TH2D("OACloseRelProbePT", "OACloseRelProbePT", n_ptBins, (&(*(ptBins.begin()))), 2000, 0, 0.2);


  if (theFilter->GetMode() == "Photon") {
    m_TH2["ResponseLoose"]   = new TH2D("hResponseLNTProbePT",  "hResponseLNTProbePT",  n_gProbePtBins, gProbePtBins, 700, -3,4);
    for (unsigned int i = 0; i < n_gProbePtBins; i++) {
      string name = "IsEMIsolCone_" + to_string((int)gProbePtBins[i]) + "_to_" + to_string((int)gProbePtBins[i+1]) + "_GeV";
      m_TH2[name.c_str()]=new TH2D(name.c_str(), name.c_str(), 2,0,2,2,0,2); //240, -20, 100, 2, 0, 2);
    }
  }

  // ////////////////////////////////////////////////////////////
  // This is for the JES systematic tool, which uses these 
  // TH2Bootstraps.  They just get filled like a normal 
  // histogram but for each event added m_nToys extra histograms
  // are similarly filled with the same events with an additional 
  // weight pulled from a poison with a mean of 1.
  //
  // For more info see talk here:
  // https://indico.cern.ch/event/316016/
  // /////////////////////////////////////////////////////////////
  
  systNames = vectorizeStr("nominal, Veto__1up, Veto__1down, dPhi__1up, dPhi__1down, JVT__1down, JVT__1up");
  vector<CP::SystematicSet> sysList = theFilter->GetsysList();
  vector<CP::SystematicSet>::const_iterator sysListItr;
  for (sysListItr = sysList.begin(); sysListItr != sysList.end(); ++sysListItr) systNames.push_back( (*sysListItr).name());
  Int_t m_nToys = theFilter->numPseudoExp;
  m_generator  = new BootstrapGenerator("Gen", "Gen", m_nToys);

  const int nRespBins = 350;
  const double minResp = -3;
  const double maxResp = 4;
  double respBins[nRespBins+1];
  for (int i = 0; i <= nRespBins; ++i) respBins[i] = minResp + (double)i * (maxResp - minResp)/nRespBins;

  for(vector<string>::const_iterator i = systNames.begin(); i != systNames.end(); ++i) {
    if (theFilter->doSystsMPF){
      string tmp =  "bootstrap_mpf_" + (*i);
      const char * c = tmp.c_str();
      TH2DBootstrap *bootstrap = new TH2DBootstrap(c, 
                                                   ";p_{T}^{ref} [GeV];Response",
                                                    ptBins.size() - 1, &ptBins[0],
                                                    nRespBins, respBins,
                                                    m_nToys,
                                                    m_generator);
      m_data["mpf_"+(*i)] = bootstrap;
    }
    // //////////////////////////////////////////////////
    // Making sure the option to do systs. for Pt balance
    // is avalible.
    // //////////////////////////////////////////////////
    if (theFilter->doSystsBal){
      string tmp =  "bootstrap_bal_" + (*i);
      const char * b = tmp.c_str();
      TH2DBootstrap *bootstrap2 = new TH2DBootstrap(b,
                                                   ";p_{T}^{ref} [GeV];Response",
                                                    ptBins.size() - 1, &ptBins[0],
                                                    nRespBins, respBins,
                                                    m_nToys,
                                                    m_generator);
      m_data["bal_"+(*i)] = bootstrap2;
    }
  }


 
  //////////////////////////////////////
  // Add histograms to job and set Sumw2
  //////////////////////////////////////
  map<string,TH1*>::iterator h1Itr = m_TH1.begin();
  map<string,TH1*>::iterator h1ItrE = m_TH1.end();
  for(; h1Itr != h1ItrE; ++h1Itr){
    TH1* h1 = (*h1Itr).second;
    h1->Sumw2();
    wk()->addOutput(h1);
  }

  map<string,TH2*>::iterator h2Itr =  m_TH2.begin();
  map<string,TH2*>::iterator h2ItrE = m_TH2.end();
  for(; h2Itr != h2ItrE; ++h2Itr){
    TH2* h2 = (*h2Itr).second;
    h2->Sumw2();
    wk()->addOutput(h2);
  }

  map<string,TH3*>::iterator h3Itr =  m_TH3.begin();
  map<string,TH3*>::iterator h3ItrE = m_TH3.end();
  for(; h3Itr != h3ItrE; ++h3Itr){
    TH3* h3 = (*h3Itr).second;
    h3->Sumw2();
    wk()->addOutput(h3);
  }

  map<string,TH2DBootstrap*>::iterator h4Itr =  m_data.begin();
  map<string,TH2DBootstrap*>::iterator h4ItrE = m_data.end();
  for(; h4Itr != h4ItrE; ++h4Itr){
    TH2DBootstrap* h4 = (*h4Itr).second;
    wk()->addOutput(h4);
  }

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode JESAnalysis::fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode JESAnalysis::changeInput ()
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode JESAnalysis::initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.

  // Count events/clean events
  m_numCleanEvents = 0;
  m_eventCounter = 0;
  m_event = wk()->xaodEvent(); 

  // Additional checks to make sure no stupid error codes (even if 
  // are just telling me something succeeded) aren't skipped over. 
  CP::CorrectionCode::enableFailure();
  xAOD::TReturnCode::enableFailure();


  // /////////////////////////////////////////////
  // Initializing the trigger decision tool.
  // Ideally this would be in the event filter
  // but it does not seem to be able to see events
  // from in there
  // /////////////////////////////////////////////

  // ///////////////////////
  // The configuration tool.
  // ///////////////////////
  m_confTool = new TrigConf::xAODConfigTool( "xAODConfigTool" );
  m_confTool->setProperty( "OutputLevel", MSG::FATAL ).ignore();
  m_confTool->initialize().ignore();
  ToolHandle<TrigConf::ITrigConfigTool> configTool(m_confTool);
 
  // //////////////////////////
  // The trigger decision tool.
  // //////////////////////////
  m_tdt = new TrigDecisionTool("TrigDecTool");
  m_tdt->setProperty( "OutputLevel", MSG::FATAL ).ignore();
  m_tdt->setProperty("ConfigTool",configTool).ignore();
  m_tdt->setProperty("TrigDecisionKey","xTrigDecision").ignore();
  if (!m_tdt->initialize().isSuccess()) {
       Error("initialize()", "Failed to properly initialize the TDT. Exiting." );
  }

  // ////////////////////////
  // Pileup reweighting tool.
  // ////////////////////////

  m_pileupTool = new CP::PileupReweightingTool("PileupReweightingTool");
  vector<string> prwFiles;     
  prwFiles.push_back("$ROOTCOREBIN/data/InSituJES/Pileup/Pileup.root");
  m_pileupTool->setProperty("ConfigFiles",prwFiles).ignore();
  vector<string> lumicalcFiles;
  lumicalcFiles.push_back("$ROOTCOREBIN/data/InSituJES/Pileup/ilumicalc_histograms_None_297730-311481_OflLumi-13TeV-008.root");
  m_pileupTool->setProperty("LumiCalcFiles",lumicalcFiles).ignore();

  if (!m_pileupTool->initialize().isSuccess() ){
      Error("initialize()", "Failed to properly initialize the PileupReweightingTool. Exiting." );
      return EL::StatusCode::FAILURE;
  }


 

  WeightTool=new CP::WeightPFOTool("weightTool");

  return EL::StatusCode::SUCCESS;
}


EL::StatusCode JESAnalysis::execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  //----------------------------
  // Event information
  //--------------------------- 

  const xAOD::EventInfo* eventInfo = 0;
  if( ! m_event->retrieve( eventInfo, "EventInfo").isSuccess() ){
     Error("execute()", "Failed to retrieve event info collection. Exiting." );
     return EL::StatusCode::FAILURE;
  }
 
  if (theFilter->isMC) {
    float mcw=1;

    m_pileupTool->apply( *eventInfo, true );
//    pileupWeight=eventInfo->auxdata<float>("PileupWeight"); //m_pileupTool->getCombinedWeight( *eventInfo );
    pileupWeight=m_pileupTool->getCombinedWeight( *eventInfo );
    cout << pileupWeight << endl;
    //pileupWeight=m_pileupTool->getCombinedWeight( *eventInfo );
    if (pileupWeight!=pileupWeight) pileupWeight=0;
    double sampleWeight = 3/fb;
    int sampleNum = eventInfo->mcChannelNumber();

    // ///////////////////////////////////////////////
    // There's probably some smarter way to do this MC 
    // slice weighting but I'm not sure what it is.
    // ///////////////////////////////////////////////

    // MC15 Pythia8 gamma+jet slices
    if      (sampleNum == 423099) sampleWeight =172.340*mb* 3.1740E-05/ 2954700;
    else if (sampleNum == 423100) sampleWeight = 16.715*mb* 2.8582E-05/ 2225200; 
    else if (sampleNum == 423101) sampleWeight =  1.397*mb* 2.4961E-05/ 2225200; //2244800;
    else if (sampleNum == 423102) sampleWeight =379.540*ub* 2.6616E-05/ 2997940; //2963340;
    else if (sampleNum == 423103) sampleWeight =106.210*ub* 3.9049E-05/ 2889000; //2885800;
    else if (sampleNum == 423104) sampleWeight =  6.705*ub* 4.8927E-05/ 998200;  //996400;
    else if (sampleNum == 423105) sampleWeight =344.260*nb* 5.6152E-05/ 996800;  //967199;
    else if (sampleNum == 423106) sampleWeight = 23.729*nb* 5.2447E-05/ 997200;  //993184;
    else if (sampleNum == 423107) sampleWeight =  2.287*nb* 3.4994E-05/ 95000; 
    else if (sampleNum == 423108) sampleWeight =702.030*pb* 3.6900E-05/ 99500; 
    else if (sampleNum == 423109) sampleWeight = 39.345*pb* 3.6688E-05/ 99300; 
    else if (sampleNum == 423110) sampleWeight =  3.328*pb* 3.3896E-05/ 99700; 
    else if (sampleNum == 423111) sampleWeight =324.950*fb* 2.9921E-05/ 99300; 
    else if (sampleNum == 423112) sampleWeight = 31.480*fb* 2.8373E-05/ 98700; 
 
    // MC15 Sherpa single photon samples
    else if (sampleNum == 343279) sampleWeight =229268.0845 / 20000000;
    else if (sampleNum == 343281) sampleWeight =250711.0689 / 9755000;
    else if (sampleNum == 343283) sampleWeight =39450.74773 / 2000000;
    else if (sampleNum == 361039) sampleWeight =4354.87664  / 20000000;
    else if (sampleNum == 361040) sampleWeight =17006.34474 / 10000000;
    else if (sampleNum == 361041) sampleWeight =3763.41504  / 2000000;
    else if (sampleNum == 361042) sampleWeight =1250.3484   / 2000000;
    else if (sampleNum == 361043) sampleWeight =1510.026471 / 2000000;
    else if (sampleNum == 361044) sampleWeight =367.696256  / 500000;
    else if (sampleNum == 361045) sampleWeight =101.5593309 / 10000000;
    else if (sampleNum == 361046) sampleWeight =124.3975876 / 2000000;
    else if (sampleNum == 361047) sampleWeight =32.1038938  / 1000000; 
    else if (sampleNum == 361048) sampleWeight =5.70950432  / 1000000; 
    else if (sampleNum == 361049) sampleWeight =6.44751333  / 500000; 
    else if (sampleNum == 361050) sampleWeight =1.95193306  / 100000; 
    else if (sampleNum == 361051) sampleWeight =0.352955948 / 200000;
    else if (sampleNum == 361052) sampleWeight =0.434633838 / 100000;
    else if (sampleNum == 361053) sampleWeight =0.138955321 / 20000;
    else if (sampleNum == 361054) sampleWeight =0.006826476 / 100000;
    else if (sampleNum == 361055) sampleWeight =0.008585541 / 50000;
    else if (sampleNum == 361056) sampleWeight =0.002999935 / 10000;
    else if (sampleNum == 361057) sampleWeight =2.9266e-05  / 101000;
    else if (sampleNum == 361058) sampleWeight =3.7468e-05  / 51000;
    else if (sampleNum == 361059) sampleWeight =1.388e-05   / 11000; 
    else if (sampleNum == 361060) sampleWeight =  2.484*zb* 4.0351E-01/ 1000;
    else if (sampleNum == 361061) sampleWeight =  2.513*zb* 4.1614E-01/ 800;
    else if (sampleNum == 361062) sampleWeight =  2.543*zb* 1.4831E-01/ 1000;


   // MC15 Sherpa Z samples
   // to Electrons
    else if (sampleNum == 361372) sampleWeight = 2.207*nb* 7.7881E-01/ 610002;
    else if (sampleNum == 361373) sampleWeight = 2.205*nb* 1.4850E-01/ 199675;
    else if (sampleNum == 361374) sampleWeight = 2.206*nb* 7.8658E-02/ 181712;
    else if (sampleNum == 361375) sampleWeight = 75.852*pb*6.4406E-01/ 189315;
    else if (sampleNum == 361376) sampleWeight = 76.023*pb*2.1240E-01/ 79689;
    else if (sampleNum == 361377) sampleWeight = 76.095*pb*1.0996E-01/ 37681;
    else if (sampleNum == 361378) sampleWeight = 11.689*pb*6.0380E-01/ 55428;
    else if (sampleNum == 361379) sampleWeight = 11.502*pb*2.3818E-01/ 15303;
    else if (sampleNum == 361380) sampleWeight = 11.653*pb*1.3304E-01/ 5192;
    else if (sampleNum == 361381) sampleWeight =767.310*fb*5.7726E-01/ 332448;
    else if (sampleNum == 361382) sampleWeight =762.770*fb*2.5177E-01/ 201451;
    else if (sampleNum == 361383) sampleWeight =765.420*fb*1.4354E-01/ 202458;
    else if (sampleNum == 361384) sampleWeight = 46.160*fb*5.6388E-01/ 230178;
    else if (sampleNum == 361385) sampleWeight = 46.402*fb*2.8977E-01/ 153350;
    else if (sampleNum == 361386) sampleWeight = 46.470*fb*5.2003E-01/ 76819;
    else if (sampleNum == 361387) sampleWeight =  8.134*fb*5.5737E-01/ 39803;
    else if (sampleNum == 361388) sampleWeight =  8.124*fb*1.7807E-01/ 23829;
    else if (sampleNum == 361389) sampleWeight =  8.156*fb*1.2095E-01/ 23755;
    else if (sampleNum == 361390) sampleWeight =  1.042*fb*5.1007E-01/ 22995;
    else if (sampleNum == 361391) sampleWeight =  1.062*fb*2.6222E-01/ 15202;
    else if (sampleNum == 361392) sampleWeight =  1.060*fb*1.3681E-01/ 15366;
    else if (sampleNum == 361393) sampleWeight =  4.278*ab*6.2209E-01/ 4025;
    else if (sampleNum == 361394) sampleWeight =  4.339*ab*3.1994E-01/ 2040;
    else if (sampleNum == 361395) sampleWeight =  4.148*ab*1.8906E-14/ 2078;


    // to Muons
    else if (sampleNum == 361396) sampleWeight =  2.206*nb* 7.7825E-01/ 833627;
    else if (sampleNum == 361397) sampleWeight =  2.205*nb* 1.4131E-01/ 519080;
    else if (sampleNum == 361398) sampleWeight =  2.204*nb* 7.8419E-02/ 1190251;
    else if (sampleNum == 361399) sampleWeight = 75.904*pb* 6.4461E-01/ 730165;
    else if (sampleNum == 361400) sampleWeight = 76.117*pb* 2.1464E-01/ 173130;
    else if (sampleNum == 361401) sampleWeight = 76.107*pb* 1.2272E-01/ 47824;
    else if (sampleNum == 361402) sampleWeight = 11.628*pb* 6.0263E-01/ 107388;
    else if (sampleNum == 361403) sampleWeight = 11.661*pb* 2.3490E-01/ 18448 ;
    else if (sampleNum == 361404) sampleWeight = 11.652*pb* 1.3978E-01/ 6174 ;
    else if (sampleNum == 361405) sampleWeight =768.990*fb* 5.7151E-01/ 377709;
    else if (sampleNum == 361406) sampleWeight =756.520*fb* 2.4999E-01/ 228309;
    else if (sampleNum == 361407) sampleWeight =771.600*fb* 1.3430E-01/ 229765;
    else if (sampleNum == 361408) sampleWeight = 46.017*fb* 5.6192E-01/ 249413;
    else if (sampleNum == 361409) sampleWeight = 46.560*fb* 3.7766E-01/ 166079;
    else if (sampleNum == 361410) sampleWeight = 46.356*fb* 1.4486E-01/ 166414;
    else if (sampleNum == 361411) sampleWeight =  8.039*fb* 5.1379E-01/ 42394;
    else if (sampleNum == 361412) sampleWeight =  7.892*fb* 2.6046E-01/ 25399;
    else if (sampleNum == 361413) sampleWeight =  8.057*fb* 1.3983E-01/ 25566;
    else if (sampleNum == 361414) sampleWeight =  1.144*fb* 4.9684E-01/ 25422;
    else if (sampleNum == 361415) sampleWeight =  1.042*fb* 1.1406E-01/ 16811;
    else if (sampleNum == 361416) sampleWeight =  1.066*fb* 1.6692E-01/ 16297;
    else if (sampleNum == 361417) sampleWeight =  4.111*ab* 5.4315E-01/ 16371;
    else if (sampleNum == 361418) sampleWeight =  4.111*ab* 1.6756E-01/ 8215;
    else if (sampleNum == 361419) sampleWeight =  4.222*ab* 9.3909E-02/ 8030;

    // Sherpa samples with NNPDF 
    // To Muons
    else if (sampleNum == 363364) sampleWeight = 1642.54477535/ 3000000;
    else if (sampleNum == 363365) sampleWeight = 240.030832472/ 1999500;
    else if (sampleNum == 363366) sampleWeight = 142.586650552/ 2000000;
    else if (sampleNum == 363367) sampleWeight = 46.639099307 / 1250000;
    else if (sampleNum == 363368) sampleWeight = 14.009508576 / 800000;
    else if (sampleNum == 363369) sampleWeight = 8.904736706  / 1077500;
    else if (sampleNum == 363370) sampleWeight = 6.776899365  / 800000;
    else if (sampleNum == 363371) sampleWeight = 2.407495134  / 800000;
    else if (sampleNum == 363372) sampleWeight = 1.557995219  / 1000000;
    else if (sampleNum == 363373) sampleWeight = 0.481671305  / 500000;
    else if (sampleNum == 363374) sampleWeight = 0.195283767  / 298600;
    else if (sampleNum == 363375) sampleWeight = 0.123565964  / 300000;
    else if (sampleNum == 363376) sampleWeight = 0.030237082  / 300000;
    else if (sampleNum == 363377) sampleWeight = 0.01296887   / 200000;
    else if (sampleNum == 363378) sampleWeight = 0.007654784  / 200000;
    else if (sampleNum == 363379) sampleWeight = 0.005363995  / 50000;
    else if (sampleNum == 363380) sampleWeight = 0.002371048  / 30000;
    else if (sampleNum == 363381) sampleWeight = 0.001495305  / 30000;
    else if (sampleNum == 363382) sampleWeight = 0.00069072   / 30000;
    else if (sampleNum == 363383) sampleWeight = 0.000318973  / 20000;
    else if (sampleNum == 363384) sampleWeight = 0.000184686  / 20000;
    else if (sampleNum == 363385) sampleWeight = 2.451e-06    / 20000;
    else if (sampleNum == 363386) sampleWeight = 1.38e-06     / 10000;
    else if (sampleNum == 363387) sampleWeight = 6.69e-07     / 10000;

    // To Electrons
    else if (sampleNum == 363388) sampleWeight = 1641.4628707/ 3000000;
    else if (sampleNum == 363389) sampleWeight = 241.466440999/2000000;
    else if (sampleNum == 363390) sampleWeight = 140.517436867/2000000;
    else if (sampleNum == 363391) sampleWeight = 46.790575075/ 1250000;
    else if (sampleNum == 363392) sampleWeight = 14.049268454/ 800000;
    else if (sampleNum == 363393) sampleWeight = 8.861525949 / 1100000; 
    else if (sampleNum == 363394) sampleWeight = 6.789837772 / 800000;
    else if (sampleNum == 363395) sampleWeight = 2.42660918  / 800000;
    else if (sampleNum == 363396) sampleWeight = 1.559604271 / 1000000;    
    else if (sampleNum == 363397) sampleWeight = 0.48716142  / 500000;
    else if (sampleNum == 363398) sampleWeight = 0.198314112 / 281000;
    else if (sampleNum == 363399) sampleWeight = 0.123000982 / 300000;
    else if (sampleNum == 363400) sampleWeight = 0.02875511  / 300000;
    else if (sampleNum == 363401) sampleWeight = 0.012936803 / 200000;
    else if (sampleNum == 363402) sampleWeight = 0.007968854 / 200000;
    else if (sampleNum == 363403) sampleWeight = 0.005414517 / 50000;
    else if (sampleNum == 363404) sampleWeight = 0.002400844 / 30000;
    else if (sampleNum == 363405) sampleWeight = 0.001494724 / 30000;
    else if (sampleNum == 363406) sampleWeight = 0.000697776 / 30000;
    else if (sampleNum == 363407) sampleWeight = 0.00033278  / 20000;
    else if (sampleNum == 363408) sampleWeight = 0.000189395 / 20000;
    else if (sampleNum == 363409) sampleWeight = 2.524e-06   / 20000;
    else if (sampleNum == 363410) sampleWeight = 1.218e-06   / 10000;
    else if (sampleNum == 363411) sampleWeight = 6.99e-07    / 10000;

    // Madgraph Pythia Z+jet, just for fun
    else if (sampleNum == 361500) sampleWeight = 1726.7712   / 6890000;
    else if (sampleNum == 361501) sampleWeight = 261.17168   / 3600000;
    else if (sampleNum == 361502) sampleWeight = 82.91976    / 2550000;
    else if (sampleNum == 361503) sampleWeight = 23.012528   / 635000;
    else if (sampleNum == 361504) sampleWeight = 8.982512    / 225000; 
    else if (sampleNum == 361505) sampleWeight = 1727.264    / 6890000;
    else if (sampleNum == 361506) sampleWeight = 261.1224    / 3600000; 
    else if (sampleNum == 361507) sampleWeight = 82.978896   / 2550000; 
    else if (sampleNum == 361508) sampleWeight = 22.955856   / 635000l; 
    else if (sampleNum == 361509) sampleWeight = 8.9952016   / 225000; 

    
 


    m_TH1["SumWeights"]->Fill(sampleNum, pileupWeight*mcw);
    pileupWeight *= sampleWeight * mcw;
  } else {
    pileupWeight = 1.;
  } 
  m_TH1["MPF_CutInfo"]->Fill(-1, pileupWeight);
  m_TH1["Bal_CutInfo"]->Fill(-1, pileupWeight);

  m_TH1["Weights"]->Fill(pileupWeight);

  m_eventCounter++;

  // //////////////////////////////////////////////////////////////////
  // I've tried to keep JESAnalysis.cxx for just filling histograms 
  // and doing a few last little things like checking the purity. 
  // PassMPF returns false if the event passes even the loosest version
  // of the cuts used in systematic evaluation.  What I've called 
  // the cutmasks are vectors of bools where each bool correspinds 
  // to whether the event passed or failed a given event. 
  // //////////////////////////////////////////////////////////////////
  
  bool PassMPF = theFilter->PassVJet(m_event);
  bool PassBal = false;
  vector<bool> BalanceMask = theFilter->GetBalanceMask();
  PassMPF = Pass(theFilter->CutMask, "nominal"); 
  PassBal = Pass(BalanceMask, "nominal");

  const xAOD::MissingETContainer* MET = 0;
  if ( !m_event->retrieve( MET, theFilter->METCollection ).isSuccess() ){ // retrieve arguments: container type, container key
    Error("execute()", "Failed to retrieve  MET container. Exiting." );
    return EL::StatusCode::FAILURE;
  }
  xAOD::MissingETContainer::const_iterator met_itr = MET->begin();

  TLorentzVector Ref = theFilter->Ref;
  TLorentzVector leadingJet = theFilter->leadingJet;

/////////////////////////////////////////////////////////////////////////
//   Use these lines to print a full list of HLT triggers in the sample
//
//   There's been some issues with triggers used in producing derivations
//   so these lines can be helpful to confirm all is right.
//
/////////////////////////////////////////////////////////////////////////
// examine the HLT_g* chains, see if they passed/failed and their total prescale
/*
  cout << endl << endl;
  auto chainGroup = m_tdt->getChainGroup("HLT_g.*loose*.*");
  std::map<std::string,int> triggerCounts;
  for(auto &trig : chainGroup->getListOfTriggers()) {
    auto cg = m_tdt->getChainGroup(trig);
    string thisTrig = trig;
    size_t found =thisTrig.find("_g");
      if (found!=std::string::npos){
      size_t endOfPt=thisTrig.find("_", found+1);
      string triggerPt=thisTrig.substr (found+2,endOfPt-found-2);
      if (Ref.Pt()/GeV > (stoi(triggerPt)+5) and cg->getPrescale()>0){
        cout << thisTrig.c_str() << " " << cg->isPassed() << " " << cg->getPrescale() << endl;
      }
    }
  } // end for loop (c++11 style) over chain group matching "HLT_g*"
*/
///////////////////////////////////////////////////////////////////////////


  // /////////////////////////////////////////////////////////
  // In the past a binned trigger was used in gamma+jet, 
  // now I'm just doing an or of all fully efficient triggers.  
  // /////////////////////////////////////////////////////////
  
  bool passTrig = false;
  vector<pair<string, pair<double, double>>> triggerBins = theFilter->GetTriggerBins();
  vector<pair<string, pair<double, double>>>::iterator trigIt =triggerBins.begin();
  if (theFilter->GetMode() == "Photon"){
    double TriggerWeight=1;
    bool isPre=true;
    for(; trigIt != triggerBins.end(); ++trigIt) {
      string trigName = (*trigIt).first +"*";  
      auto cg = m_tdt->getChainGroup(trigName);     
      if (Ref.Pt()/GeV  > (*trigIt).second.first and Ref.Pt()/GeV < (*trigIt).second.second and m_tdt->getPrescale((*trigIt).first) >0){
  	if (m_tdt->getPrescale((*trigIt).first )>1) TriggerWeight*=(1-1/m_tdt->getPrescale((*trigIt).first ));
        else isPre=false;
        if (m_tdt->isPassed((*trigIt).first )) {
          passTrig = true;
        }
      }
    }
    if (TriggerWeight !=1 and isPre) pileupWeight*=1/(1-TriggerWeight);
    }
  else {
    for(; trigIt != triggerBins.end(); ++trigIt) {
      if (m_tdt->isPassed((*trigIt).first )) {
        passTrig = true;
      }
    }
  }


  if (theFilter->isMC) passTrig = true;
  if (not theFilter->isMC and not passTrig){ 
    theFilter->Fail("Trigger");
    PassMPF = PassBal = false;
  }

  // ///////////////////////////////////////////////////////////
  // The difference between the measured response using 
  // egamma objects and muons seems to come from the 
  // energy of the egamma objects being under represented
  // in clusters, even at uncalibrated scale, by a few percent
  // (Pt dependent).  Egamma objects are 3X5 sliding windows (in
  // the second samples layer of the EM cal) and not topo clusters
  // but still I blindly wouldn't expect this to be the case.  
  // The being said the current fix is removing the topo clusters 
  // from the MET (at the relevant scale) and repling them with the 
  // uncalibrated egamma object.  In recent derivations the cluster
  // collection was removed from JETM4.  Using derivations with
  // topoclusters it looks like I get the exact same thing using the 
  // MET builder tool, setting the jetPt to some high value and 
  // only adding the leading photon to the MET maker.  This makes 
  // sense, the thing that I'm struggling to understand is why this 
  // same move doesn't work for Z->ee.  
  // //////////////////////////////////////////////////////////////

  double PxCal=0;
  double PxEM=0;
  double PyCal=0;
  double PyEM=0;
  double PxEM1=0;
  double PxEM2=0;
  double PxCal1=0;
  double PxCal2=0;


  // /////////////////////////////////////////////////////////
  // There seems to be some issue with the MET overlap removal
  // of photons coming from the smoothing being done to the 
  // track->cluster transition.  For a quick fix I'm just 
  // going to rebuild a MET for Z+jet events but this is going 
  // to need to be fixed in the future. 
  // ///////////////////////////////////////////////////////////
  double PFlowMETx=0;
  double PFlowMETy=0;
/*  if((theFilter->JetCollection == "AntiKt4EMPFlowJets") and not theFilter->isPhoton){
    const xAOD::PFOContainer* neutralPFO=0;
    if ( !m_event->retrieve( neutralPFO, "JetETMissNeutralParticleFlowObjects" ).isSuccess() ){ // retrieve arguments: container type, container key
      Error("execute()", "Failed to retrieve PFO container. Exiting." );
      return EL::StatusCode::FAILURE;
    }
   
    xAOD::PFOContainer::const_iterator pfo_itr = neutralPFO->begin();
    for(pfo_itr = neutralPFO->begin();pfo_itr != neutralPFO->end(); ++pfo_itr ){
      if( (*pfo_itr)->eEM() >0){
        PFlowMETx-=(*pfo_itr)->p4EM().Px();
        PFlowMETy-=(*pfo_itr)->p4EM().Py();
        bool Lep1=false;
        bool Lep2=false;
        if ((*pfo_itr)->p4().DeltaR(theFilter->Lep1) < 0.2) Lep1=true; 
        if ((*pfo_itr)->p4().DeltaR(theFilter->Lep2) < 0.2) Lep2=true; 
        if (Lep1 or Lep2){
          PxEM+=(*pfo_itr)->p4EM().Px();
          PyEM+=(*pfo_itr)->p4EM().Py();
        } 
        if (Lep1) PxEM1+= (*pfo_itr)->p4().Px();
        if (Lep2) PxEM2+= (*pfo_itr)->p4().Px();
      }
    }
    const xAOD::PFOContainer* chargedPFO=0;
    if ( !m_event->retrieve( chargedPFO, "JetETMissChargedParticleFlowObjects" ).isSuccess() ){ // retrieve arguments: container type, container key
      Error("execute()", "Failed to retrieve PFO container. Exiting." );
      return EL::StatusCode::FAILURE;
    }

    pfo_itr = chargedPFO->begin();
    for(pfo_itr = chargedPFO->begin();pfo_itr != chargedPFO->end(); ++pfo_itr ){
      float weight=0;
      WeightTool->fillWeight((**pfo_itr), (weight)).ignore();
      PFlowMETx-=(*pfo_itr)->p4EM().Px()*weight;
      PFlowMETy-=(*pfo_itr)->p4EM().Py()*weight;
      bool Lep1=false;
      bool Lep2=false;
      if ((*pfo_itr)->p4().DeltaR(theFilter->Lep1) < 0.2) Lep1=true;
      if ((*pfo_itr)->p4().DeltaR(theFilter->Lep2) < 0.2) Lep2=true;
      if (Lep1 or Lep2){
        PxEM+=(*pfo_itr)->p4EM().Px()*(weight);
        PyEM+=(*pfo_itr)->p4EM().Py()*(weight);
      }
      if (Lep1) PxCal1+= (*pfo_itr)->p4().Px();
      if (Lep2) PxCal2+= (*pfo_itr)->p4().Px();
    }
  }
*/
  // ///////////////////////////////////////////////////////////////////////////////////
  // Topoclusters seem to have less energy than their sliding window counterparts.  
  // To get MET with electrons to agree with the MET with muons I'm taking the 
  // topoclusters near the electrons out of the MET and adding in the electrons
  // themselves.  This is necessarily going to add some small pileup dependence
  // as this involves removing any pileup that happened to have land in the topocluster
  // but not in the sliding window, so to help Z->ee Z->mumu agreement I'm doing
  // the same to any topoclusters near muons. 
  // /////////////////////////////////////////////////////////////////////////////////// 
  if (false) { //not theFilter->isPhoton){  
    if(not (theFilter->JetCollection=="AntiKt4EMPFlowJets")){
      // Cluster correction used in 2015 to correct for mismatch
      // Topoclusters and EM clusters for electrons. 
      const xAOD::CaloClusterContainer* clusters = 0;
      if ( !m_event->retrieve( clusters, "CaloCalTopoClusters" ).isSuccess() ){ // retrieve arguments: container type, container key
        Error("execute()", "Failed to retrieve  Cluster container. Exiting." );
        return EL::StatusCode::FAILURE;
      }
      xAOD::CaloClusterContainer::const_iterator clus_itr = clusters->begin();
 
      for(clus_itr = clusters->begin();clus_itr != clusters->end(); ++clus_itr ){
        bool Lep1=false;
        bool Lep2=false;
        if ((*clus_itr)->p4(xAOD::CaloCluster_v1::State::UNCALIBRATED).DeltaR(theFilter->Lep1) < 0.2)Lep1=true;
        if ((*clus_itr)->p4(xAOD::CaloCluster_v1::State::UNCALIBRATED).DeltaR(theFilter->Lep2) < 0.2)Lep2=true;
        if (Lep1 or Lep2){
          PxCal+=(*clus_itr)->p4(xAOD::CaloCluster_v1::State::CALIBRATED).Px();
          PyCal+=(*clus_itr)->p4(xAOD::CaloCluster_v1::State::CALIBRATED).Py();
          PxEM+=(*clus_itr)->p4(xAOD::CaloCluster_v1::State::UNCALIBRATED).Px();
          PyEM+=(*clus_itr)->p4(xAOD::CaloCluster_v1::State::UNCALIBRATED).Py();

          if (Lep1) PxEM1+= (*clus_itr)->p4(xAOD::CaloCluster_v1::State::UNCALIBRATED).Px();
          if (Lep1) PxCal1+= (*clus_itr)->p4(xAOD::CaloCluster_v1::State::CALIBRATED).Px();
          if (Lep2) PxEM2+=  (*clus_itr)->p4(xAOD::CaloCluster_v1::State::UNCALIBRATED).Px();
          if (Lep2) PxCal2+=  (*clus_itr)->p4(xAOD::CaloCluster_v1::State::CALIBRATED).Px();
        }
      }
    }
  }
  else if (theFilter->isPhoton) {
    PxEM=theFilter->UncalLep1.Px(); 
    PyEM=theFilter->UncalLep1.Py(); 
  }
  else {
    PxEM=theFilter->UncalLep1.Px()+theFilter->UncalLep2.Px();
    PyEM=theFilter->UncalLep1.Py()+theFilter->UncalLep2.Py();
  }
  

  if (PassMPF){
    m_TH2["ClusOverUncalEl"]->Fill( theFilter->Lep1.Pt()/GeV, PxEM1/theFilter->UncalLep1.Px());
    m_TH2["ClusOverEl"]->Fill( theFilter->Lep1.Pt()/GeV, PxEM1/theFilter->Lep1.Px());
    m_TH2["CalClusOverUncalEl"]->Fill( theFilter->Lep1.Pt()/GeV, PxCal1/theFilter->UncalLep1.Px());
    m_TH2["CalClusOverEl"]->Fill( theFilter->Lep1.Pt()/GeV, PxCal1/theFilter->Lep1.Px());
    m_TH2["CalElOverUncalEl"]->Fill(Ref.Eta(), PxEM/theFilter->corrX);
    if (not theFilter->isPhoton){
      m_TH2["ClusOverUncalEl"]->Fill( theFilter->Lep2.Pt()/GeV, PxEM2/theFilter->UncalLep2.Px());
      m_TH2["ClusOverEl"]->Fill( theFilter->Lep2.Pt()/GeV, PxEM2/theFilter->Lep2.Px());
      m_TH2["CalClusOverUncalEl"]->Fill( theFilter->Lep2.Pt()/GeV, PxCal2/theFilter->UncalLep2.Px());
      m_TH2["CalClusOverEl"]->Fill( theFilter->Lep2.Pt()/GeV, PxCal2/theFilter->Lep2.Px());
    }
  }

  if (not theFilter->isPhoton and theFilter->JetCollection == "AntiKt4LCTopoJets") {
    PxEM=PxCal;
    PyEM=PyCal;
  }

  double METx = (*met_itr)->mpx();
  double METy = (*met_itr)->mpy();

 
  double METCorrY = 0;
  double METCorrX = 0;

  // ////////////////////////////////////////////////////
  // Photons probably also have the same bias to
  // to lower response due to cluster/sliding window
  // mismatch but since this problem would exist in 
  // both data and MC and it's only the ratio we care 
  // about I'm fine using the MET from the METMaker tool
  // for photons. 
  // /////////////////////////////////////////////////////
  if (theFilter->isPhoton){
    METx = theFilter->METUtilx;
    METy = theFilter->METUtily;
  }
  else if (theFilter->JetCollection == "AntiKt4EMPFlowJets"){
//    cout << PFlowMETx << " " << theFilter->METUtilx << endl;
    METx = theFilter->METUtilx; //PFlowMETx;
    METy = theFilter->METUtily; //PFlowMETy;
  }


  // /////////////////////////////////////////////////////////////
  // Correcting the MET for mismatch between EM and topo clusters, 
  // as well as propagating the GSC correction through to the MET 
  // in the way that was decided on in 2015.
  // /////////////////////////////////////////////////////////////


  double METx_NoGSC = METx - Ref.Px() + PxEM;
  double METy_NoGSC = METy - Ref.Py() + PyEM;

  double MPF_NoGSC = Ref.Px()*METx_NoGSC+Ref.Py()*METy_NoGSC;
  MPF_NoGSC/=(Ref.Pt() * Ref.Pt());
  MPF_NoGSC+=1;


  METCorrX = Ref.Px() - PxEM - (1-theFilter->leadingJetGSC)*theFilter->leadingJetConst.Px(); 
  METCorrY = Ref.Py() - PyEM - (1-theFilter->leadingJetGSC)*theFilter->leadingJetConst.Py(); 

  METx -= METCorrX;
  METy -= METCorrY;
  
  double MPF = Ref.Px() * METx + Ref.Py() * METy;
  MPF /= (Ref.Pt() * Ref.Pt());
  ++MPF;

  if(PassMPF){
    m_TH1["METx"]->Fill(METx/GeV);
    m_TH1["METy"]->Fill(METy/GeV);
  }
  // //////////////////
  // Purity measurement
  // //////////////////
  if((theFilter->GetMode() == "Photon" and theFilter->isTight != -1)) {
    if ((PassMPF or Pass(theFilter->CutMask, "Pure")) and passTrig){
      if (not PassMPF and Pass(theFilter->CutMask, "Photon: tight")){
        m_TH2["ResponseLoose"]->Fill(Ref.Pt()/1000, MPF, pileupWeight);
      }
      int bin=0;
      for (; bin <= (int)n_gProbePtBins; bin++) if (Ref.Pt()/GeV <  ptBins[bin+1]) break;
      string name = "IsEMIsolCone_" + to_string((int)ptBins[bin]) + "_to_" + to_string((int)ptBins[bin+1]) + "_GeV";
      if (not (Ref.Pt()/GeV < ptBins[0]) and (ptBins[bin+1] > 0)){
        if (theFilter->objectFilter->isTight >=0) m_TH2[name.c_str()]->Fill(theFilter->objectFilter->isIsol, theFilter->isTight, pileupWeight);
      }
    }
  }


  // ////////////////////////////////////////////////////////////////////////
  // These vectors contain a TLorenz vector with the shifted reference object
  // as well as the name of the systematic variation.  For variations of cuts 
  // like the subleading jet cut and the delta phi cut those names only 
  // appear in these vectors if the event passed the varied cut, and the 
  // vector is the nominally calibrated reference object. 
  // ////////////////////////////////////////////////////////////////////////

  vector<pair<TString, TLorentzVector>> *MPFSyst = theFilter->GetMPFSyst();
  vector<pair<TString, TLorentzVector>> *BalSyst = theFilter->GetBalSyst();

  vector<pair<TString, TLorentzVector>>::const_iterator it = MPFSyst->begin();
  vector<pair<TString, TLorentzVector>>::const_iterator it_end = MPFSyst->end();


  for(;it!=it_end;++it) {
    if (passTrig and theFilter->doSystsMPF){
      double METx_scale = (*met_itr)->mpx() + Ref.Px() - (*it).second.Px() - METCorrX;
      double METy_scale = (*met_itr)->mpy() + Ref.Py() - (*it).second.Py() - METCorrY;
      double MPF_var = (*it).second.Px() * METx_scale + (*it).second.Py() * METy_scale; 
      MPF_var /= ((*it).second.Pt() * (*it).second.Pt());
      ++ MPF_var;
      char result[100];
      strcpy(result,"mpf_"); 
      strcat(result,((*it).first).Data()); 
      m_generator->Generate(eventInfo->runNumber(), eventInfo->eventNumber());
      m_data[result]->Fill((*it).second.Pt()/GeV,MPF_var,pileupWeight);
    }
  }
  
  it = BalSyst->begin();
  it_end = BalSyst->end();
  for(;it!=it_end;++it) {
    if (passTrig and theFilter->doSystsBal){
      double Bal_var = leadingJet.Pt() / (fabs( (*it).second.Pt()*cos((*it).second.DeltaPhi(leadingJet))));
      char result[100];
      strcpy(result,"bal_");
      strcat(result,((*it).first).Data());
      m_generator->Generate(eventInfo->runNumber(), eventInfo->eventNumber());
      m_data[result]->Fill((*it).second.Pt()/1000,Bal_var,pileupWeight);
    }
  }


  // ////////////////////////////////////////////////////////////////////
  // Putting in some TH3's show I can plot how the response varies as you 
  // loosen and tighten these cuts, as well as looking at how the J2 and 
  // dPhi cuts effect each other.
  // ////////////////////////////////////////////////////////////////////
  
  if (Pass(theFilter->CutMask, "Rad") and passTrig and doRadiationStudies){
    int bin=0;
    vector<double>::const_iterator it;
    for (it = ptBins.begin(); it != ptBins.end()-1; ++it, bin++) if (Ref.Pt()/GeV <  ptBins[bin+1]) break;
    if (not (Ref.Pt()/GeV < ptBins[0]) and (ptBins[bin+1] > 0)){
      string name = "ResponseVsRad_" + to_string((int)ptBins[bin]) + "_to_" + to_string((int)ptBins[bin+1]) + "_GeV";
      m_TH3[name.c_str()]->Fill(MPF,theFilter->RelSubleadingJetPt, theFilter->dPhi, pileupWeight);
    }
  }

  double Balance    = leadingJet.Pt() / (fabs( Ref.Pt()*cos(Ref.DeltaPhi(leadingJet))));
  
  // I'm filling some plots for Pt balance without applying any calibrations just to see it
  double RawBalance = theFilter->leadingJetConst.Pt() / (fabs( Ref.Pt()*cos(Ref.DeltaPhi(theFilter->leadingJetConst))));

  m_TH1["MPF_CutInfo"]->Fill(theFilter->MPF_FirstFailed, pileupWeight); 
  m_TH1["Bal_CutInfo"]->Fill(theFilter->Bal_FirstFailed, pileupWeight);
  

  if (PassMPF and theFilter->doSystsMPF){
    m_generator->Generate(eventInfo->runNumber(), eventInfo->eventNumber());
    m_data["mpf_nominal"]->Fill(Ref.Pt()/GeV,MPF,pileupWeight);
  }
  if (PassBal){
    m_TH2["Balance"]->Fill(Ref.Pt()/GeV,    Balance,    pileupWeight);
    m_TH2["RawBalance"]->Fill(Ref.Pt()/GeV, RawBalance, pileupWeight);
  }
  int bin=0;
  vector<double>::const_iterator BinIt;
  for (BinIt = ptBins.begin(); BinIt != ptBins.end()-1; ++BinIt, bin++) if (Ref.Pt()/GeV <  ptBins[bin+1]) break;
  if (not (Ref.Pt()/GeV < ptBins[0]) and doJetEtaStudies and Pass(theFilter->CutMask, "Lead jet: eta") ){
    string name = "ResponseVsJetEta_" + to_string((int)ptBins[bin]) + "_to_"+ to_string((int)ptBins[bin+1]) + "_GeV";
    m_TH3[name.c_str()]->Fill(MPF,leadingJet.Eta(), theFilter->subleadingJetEta, pileupWeight);
  }
  if (!PassMPF){
    return EL::StatusCode::SUCCESS;
   }

  // ////////////////////
  // Fill kinematic plots
  // ////////////////////

  m_TH1["JetPt"]->Fill(leadingJet.Pt()/GeV, pileupWeight); 
  m_TH1["JetEta"]->Fill(leadingJet.Eta(), pileupWeight);

  m_TH1["RefPt"] ->Fill(  Ref.Pt()/GeV, pileupWeight);
  if(not theFilter->isPhoton){
    m_TH1["LeptonPt"]->Fill(theFilter->Lep1.Pt()/GeV, pileupWeight);
    m_TH1["LeptonPt"]->Fill(theFilter->Lep2.Pt()/GeV, pileupWeight);
  }

  m_TH1["RefEta"]->Fill(  Ref.Eta(), pileupWeight); 
  m_TH1["RefMass"]->Fill( Ref.M()/GeV, pileupWeight);
 
  double rho = ( (theFilter->JetCollection.find("LC") == std::string::npos) ? 6000. : 12000.);
  const xAOD::EventShape * eventShape = 0;
  std::string m_rhoKey = ( (theFilter->JetCollection.find("LC") == std::string::npos)) ? "Kt4EMTopoEventShape" : "Kt4LCTopoEventShape";
  if ( !m_event->retrieve(eventShape, m_rhoKey).isSuccess() ){ // retrieve arguments: container type, container key
    Error("execute()", "Failed to retrieve  Cluster container. Exiting." );
    return EL::StatusCode::FAILURE;
  }


  eventShape->getDensity( xAOD::EventShape::Density, rho );
  m_TH2["MuVsRho"]->Fill(rho/GeV, eventInfo->averageInteractionsPerCrossing(), pileupWeight);


  m_numCleanEvents++;


  if (theFilter->GetMode() != "Photon") {
    m_TH1["dPhiLep1Jet"]->Fill(fabs(theFilter->Lep1.DeltaPhi(leadingJet)), pileupWeight);
    m_TH1["dPhiLep2Jet"]->Fill(fabs(theFilter->Lep2.DeltaPhi(leadingJet)), pileupWeight);
  }

  // ////////////////////////////////////////////////////////
  // The same idea as the radiation studies above, looking at 
  // MPF response Vs. mu and NPV
  // ////////////////////////////////////////////////////////

  bin=0;
  for (BinIt = ptBins.begin(); BinIt != ptBins.end()-1; ++BinIt, bin++) if (Ref.Pt()/GeV <  ptBins[bin+1]) break;
  if (not (Ref.Pt()/GeV < ptBins[0]) and doPileupStudies){
    string name = "ResponseVsPileup_" + to_string((int)ptBins[bin]) + "_to_"+ to_string((int)ptBins[bin+1]) + "_GeV";
    m_TH3[name.c_str()]->Fill(MPF,theFilter->NPV, eventInfo->averageInteractionsPerCrossing(), pileupWeight);
  }

  if (not (Ref.Pt()/GeV < ptBins[0]) and doJetDistanceStudies){
    string name = "ResponseVsJetDistances_" + to_string((int)ptBins[bin]) + "_to_"+ to_string((int)ptBins[bin+1]) + "_GeV";
    m_TH3[name.c_str()]->Fill(MPF,theFilter->deltaRnearestJet, theFilter->subleadingJet.DeltaR(theFilter->leadingJet), pileupWeight);
  }

  m_TH1["AvInt"]->Fill(eventInfo->averageInteractionsPerCrossing(), pileupWeight);
  m_TH1["NPV"]->Fill(theFilter->NPV, pileupWeight);

  // /////////////////////////////////////////////////////////////////////////////////
  // Studies looking at the effect of other activity in the event on the MPF
  // After some discussion with Mike it was noticed that the way this was done
  // in the past was wrong and that something similiar to the out of cone 
  // correction is needed to take the measured jet energy to the full measured
  // energy of the recoil.  There are some hardcoded numbers below I used for a 
  // few rough tests but to fully do these studies this stuff will need to be fixed.  
  //
  // There's also a possibility that these studies will be done using different cone
  // sizes so I'm going to have to set up the jet finder in here at some point. 
  // /////////////////////////////////////////////////////////////////////////////////
  
  m_TH2["AppliedJES"]->Fill(1/(theFilter->leadingJetGSC*theFilter->leadingJetJES), pileupWeight); 
  m_TH2["AppliedGSC"]->Fill(theFilter->leadingJetGSC, pileupWeight); 
  double OA=CalcOA(1/(theFilter->leadingJetGSC*theFilter->leadingJetJES), theFilter->leadingJetPileupCorr, Ref, false);
  m_TH1["OA"]->Fill(OA, pileupWeight); 
  m_TH2["OAVsPt"]->Fill(Ref.Pt()/GeV, OA, pileupWeight);

  double Re=FullResponseFit(Ref.Pt());
  double OAApprox=0.475-Ref.Pt()*0.00075/GeV;
  m_TH2["jApprox"]->Fill(Ref.Pt()/GeV, MPF+OAApprox, pileupWeight);
  m_TH2["OAClose"]->Fill(Ref.Pt()/GeV, ((Re-1)*Ref.Pt()-(Ref.Px() * METx + Ref.Py() * METy))/(GeV*Ref.Pt()));
  m_TH2["OACloseRel"]->Fill(Ref.Pt()/GeV, ((Re-1)*Ref.Pt()-(Ref.Px() * METx + Ref.Py() * METy))/(Ref.Pt()*Ref.Pt()));
  
 
  if (theFilter->isMC) {
    if (theFilter->TrueMF>0.5){
      double TruthResponse = theFilter->leadingJetPileupCorr.Pt()/theFilter->TrueJetPt;
      m_TH2["TrueResponse"]->Fill(Ref.Pt()/GeV,TruthResponse, pileupWeight);
      m_TH2["MPFTrueRatio"]->Fill(Ref.Pt()/GeV, TruthResponse/MPF, pileupWeight);
      m_TH2["TrueMF"]->Fill(Ref.Pt()/GeV, theFilter->TrueMF, pileupWeight);
    }
  }

  
  m_TH1["Isolation"]->Fill(theFilter->objectFilter->IsolE/GeV, pileupWeight);

  if(Ref.Pt()>150*GeV) m_TH2["Runs"]->Fill( eventInfo->runNumber(), MPF, pileupWeight);
  if(Ref.Pt()>35*GeV) m_TH2["Runs35"]->Fill( eventInfo->runNumber(), MPF, pileupWeight);

  m_TH2["Response"]->Fill(Ref.Pt()/GeV, MPF, pileupWeight);
  m_TH2["ResponseNoGSC"]->Fill(Ref.Pt()/GeV, MPF_NoGSC, pileupWeight);

  m_TH2["Mapping"]->Fill(Ref.Pt()/GeV, leadingJet.Pt()/GeV, pileupWeight);

/*
  for (int size=0; size<n_coneSizes; size++){
    allGood = ShoweringTool->GetShowering(m_event, theFilter->Lep1, theFilter->Lep2, RefId, double(coneSizes[size])/10, leadingJet,true);

    if (not allGood) break;
    double Re=PartialResponseFit(Ref.Pt());
    double dot = ShoweringTool->recoJet.Pt() * cos(Ref.DeltaPhi(ShoweringTool->recoJet));
    double OA = (Re-1)*(1+dot/(Re*Ref.Pt()));

    string ShowerString="OtherActivity"+to_string(coneSizes[size]);
    m_TH2[ShowerString]->Fill(Ref.Pt()/GeV, OA, pileupWeight);
  }
*/

  if (doRingStudies){
    int bin=0;
    vector<double>::const_iterator it;
    for (it = ptBins.begin(); it != ptBins.end()-1; ++it, bin++) if (Ref.Pt()/GeV <  ptBins[bin+1]) break;
    if (not (Ref.Pt()/GeV < ptBins[0]) and (ptBins[bin+1] > 0)){
      string name = "EnergyRing_"       + to_string((int)ptBins[bin]) + "_to_" + to_string((int)ptBins[bin+1]) + "_GeV";

      const xAOD::CaloClusterContainer* clusters=0;
      if ( !m_event->retrieve( clusters, "CaloCalTopoClusters" ).isSuccess() ){
        cout << "Failed to get CalCellInfo" << endl;
        return false;
      }
      xAOD::CaloClusterContainer::const_iterator clus_itr = clusters->begin();
      xAOD::CaloClusterContainer::const_iterator clus_end = clusters->end();

      double profile[numRings];
      double tProfile[numRings];
      for( unsigned int a=0; a < numRings; a+=1 ) {profile[a]=0;tProfile[a]=0; } //cout << a << endl;}
      double binSize=furthestRing/((double)numRings);

      for(;clus_itr!=clus_end;++clus_itr){
        if((*clus_itr)->e()>0) {
          int bin = (*clus_itr)->p4().DeltaR(leadingJet)/binSize;
          if (bin < numRings){
            //cout << bin << " " << (*clus_itr)->p4().DeltaR(leadingJet) << " " << (double)bin*binSize << " " << (*clus_itr)->rawE() << endl;
            profile[bin]+=(*clus_itr)->rawE();
          }
        }
      }
      if(theFilter->isMC){
        const xAOD::TruthParticleContainer* truths=0;
        if ( !m_event->retrieve( truths, "TruthParticles" ).isSuccess() ){
          cout << "Failed to get TruthParticles" << endl;
          return false;
        }
        xAOD::TruthParticleContainer::const_iterator truth_itr = truths->begin();
        xAOD::TruthParticleContainer::const_iterator truth_end = truths->end();
        for(;truth_itr!=truth_end;++truth_itr){
          if (fabs((*truth_itr)->px())>0.1 and (*truth_itr)->status()==1 and (*truth_itr)->barcode()<100000){
            bin = (*truth_itr)->p4().DeltaR(leadingJet)/binSize;
            if (bin < numRings){
              tProfile[bin]+=(*truth_itr)->e();
            }
          } 
        }
      }
      for( unsigned int a = 0; a < numRings; a = a + 1 ){
        m_TH2[name.c_str()]->Fill(  (double)a*binSize, profile[a]/theFilter->leadingJetPileupCorr.E(), pileupWeight);
        //double binArea = pow((a+1)*binSize, 2) - pow((a)*binSize,2);
        //binArea /= M_PI;
      }
    }
  }



  m_TH1["MPF"]->Fill(MPF, pileupWeight);
 


  MET=0;
  eventInfo=0;
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode JESAnalysis::postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode JESAnalysis::finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.
  delete WeightTool;
  if ( m_confTool ){
    delete  m_confTool;
    m_confTool=0;
  }
  if (m_tdt){
    delete m_tdt;
    m_tdt=0;
  }
  if (m_pileupTool){
  if (!m_pileupTool->finalize().isSuccess() ){
      Error("finalize()", "Failed to properly finalize the PileupReweightingTool. Exiting." );
      return EL::StatusCode::FAILURE;
  }
    delete m_pileupTool;
    m_pileupTool=0;
  }
  Info("finalize()", "Number of events passing selection = %i", m_numCleanEvents);
  
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode JESAnalysis::histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.


  for(Int_t Cut = 0; Cut<(Int_t) theFilter->NumCuts(); ++Cut){
    m_TH1["MPF_CutInfo"]->GetXaxis()->SetBinLabel(Cut+1, theFilter->GetCuts(Cut).c_str());
    m_TH1["Bal_CutInfo"]->GetXaxis()->SetBinLabel(Cut+1, theFilter->GetCuts(Cut).c_str());
  }
  delete m_generator;
  delete theFilter;
  return EL::StatusCode::SUCCESS;

}

vector<string> JESAnalysis::vectorizeStr(string bins)
{
    std::istringstream stream(bins);
    std::string bin;

    std::vector<string> output;

    while (getline(stream, bin, ',')) {
        while (bin[0] == ' ') bin.erase(bin.begin());
        output.push_back(bin);
    }
    return output;
}

// /////////////////////////////////////////////////////////
// Function that looks at the vector of cuts and determines
// if an event passed all the cuts.  The string cut is there 
// to temporarily ignore a certain cut or group of cuts. 
// /////////////////////////////////////////////////////////
bool JESAnalysis::Pass(vector<bool> Mask, string cut)
{
  bool pass = true;
  if (cut == "Rad"){
    int count = 0;
    for( vector<bool>::const_iterator i = Mask.begin(); i != Mask.end(); ++i){
      if (!(theFilter->FindCode("Veto") == count) and !(theFilter->FindCode("dPhi") == count)) pass = pass and (*i);
      count +=1 ;
    }
  }
  else if (cut == "Pure"){
    int count = 0;
    for( vector<bool>::const_iterator i = Mask.begin(); i != Mask.end(); ++i){
      if (!(theFilter->FindCode("Photon: Isolation") == count) and !(theFilter->FindCode("Photon: tight") == count)) pass = pass and (*i);
      count +=1 ;
    }
  }
  else {
    int count = 0;
    for( vector<bool>::const_iterator i = Mask.begin(); i != Mask.end(); ++i){
      if (!(theFilter->FindCode(cut) == count)) pass = pass and (*i);
      count +=1 ;
    }
  }
  return pass;
}

// ////////////////////////////////////////////////
// These last few functions are just leftovers from 
// me playing with the other activity measurement.  
// They will probably never be used again but I
// didn't see much use in removing this stuff on 
// the off chance that I do want to try this again.
// ///////////////////////////////////////////////// 


double JESAnalysis::PartialResponseFit(double RefEnergy)
{
  double a=0.627629;
  double b=0.126434;
  double c=-0.0219548;
  double d=-0.0331396;
  double E=RefEnergy/100/GeV;
  return a+b*log(E)+c*log(E)*log(E)+d*log(E)*log(E)*log(E);
}
 
double JESAnalysis::FullResponseFit(double RefEnergy)
{
double a=0.600923;
double b=0.158921;
double c=-0.0514905;
double d=0.00423454;

  double E=RefEnergy/100/GeV;
  return a+b*log(E)+c*log(E)*log(E)+d*log(E)*log(E)*log(E);
}
 
 
double JESAnalysis::CalcOA(double Resp, TLorentzVector jet, TLorentzVector Ref, bool dokTerm)
{
  // K TERM FOR ANTI KT 4 EM TOPO JETS FROM 2012
  double kTerm[]={0.830732,0.857148,0.895417,0.912491,0.929006,0.954425,0.968621,0.977318,0.976803,0.981674,0.974729};
  double zProbePtBins[]={17., 20., 25., 30., 35., 45., 60., 80., 110., 160., 210.,   260., 350., 800, 6000};
  UInt_t n_zProbePtBins=sizeof(zProbePtBins)/sizeof(double)-1;
  double kTermMeas=0;
  for (unsigned int i = 0; i <= n_zProbePtBins;i++){
    if (Ref.Pt()/GeV < zProbePtBins[i]) {
      if (i>0)  kTermMeas=kTerm[i-1];
      break;
    }
  }
 
  double stuff=Ref.Px()*jet.Px()+Ref.Py()*jet.Py();
  if (dokTerm)stuff=Ref.Px()*jet.Px()/kTermMeas+Ref.Py()*jet.Py()/kTermMeas;
  stuff/=(Resp*Ref.Pt()*Ref.Pt());
  double OA= (Resp-1)*(1+stuff);
  return OA;
}
