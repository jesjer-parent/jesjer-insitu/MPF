// ---------------------------------------------------------
#include "InSituJES/EventFilter.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"
#include "JetSelectorTools/JetCleaningTool.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODEgamma/EgammaEnums.h"
#include "xAODCaloEvent/CaloCluster.h"

#include "xAODMissingET/MissingETAuxContainer.h"
#include "xAODMissingET/MissingETAssociationMap.h"
#include "xAODMissingET/MissingETContainer.h"

#include "xAODTau/TauJetContainer.h"

// Tools
#include "GoodRunsLists/GoodRunsListSelectionTool.h"


using namespace std;


// ---------------------------------------------------------
EventFilter::EventFilter(TString configFile)
{
  TEnv * configs = new TEnv();

  int status = configs->ReadFile(configFile,EEnvLevel(0));
  if (status!=0) {
     cout<<"cannot read config file in Event Filter"<< endl;
     cout<<configFile<<endl;
     cout<<"******************************************\n"<< endl;
  }
  TString selectionFile = configs->GetValue("selectionFile", "");
  TString selectionFile2 = gSystem->ExpandPathName ("$ROOTCOREBIN/data/InSituJES/"+selectionFile);
  cout << "Selection File " <<  selectionFile2 << endl;
  TEnv * settings = new TEnv();

  status = settings->ReadFile(selectionFile2,EEnvLevel(0));
  if (status!=0) {
     cout<<"cannot read config file in Event Filter"<< endl;
     cout<<selectionFile2<<endl;
     cout<<"******************************************\n"<< endl;
  } 

  doGRL                 = configs->GetValue("doGRL", true);
  isMC                  = configs->GetValue("isMC", false);

  doSystsMPF		= configs->GetValue("doSystsMPF", false);
  doSystsBal		= configs->GetValue("doSystsBal", false);
  numPseudoExp		= configs->GetValue("numPseudoExp", 500);


  // ///////////////////////
  // Reference specific cuts
  // ///////////////////////
  leadingJet_MinPt  	= settings->GetValue("leadingJet_minPt", 0);
  ph_overlapRemoval     = settings->GetValue("ph_overlapRemoval", 0.2);
  lep_overlapRemoval    = settings->GetValue("lep_overlapRemoval", 0.35);
  Z_Min                 = settings->GetValue("Z_Mass_Min", 66000);
  Z_Max                 = settings->GetValue("Z_Mass_Max", 116000);

  isPhoton = isElectron = isMuon = false;
  mode      = settings->GetValue("Mode", "None");
  if (mode == "Electron") isElectron = true;
  else if (mode == "Muon") isMuon = true;
  else if (mode == "Photon") isPhoton = true;
  else Error("initialize()", "Must specify refence object (Mode) as Photon Electron or Muon." );
  
  // /////////////////
  // Jet specific cuts
  // /////////////////
  JetCollection         = configs->GetValue("JetCollection", "AntiKt4LCTopoJets");
  min_delta_phi         = GetSettings(settings, "min_delta_phi_MPF", 2.9);
  j2_maxRelPt           = GetSettings(settings, "j2_maxRelPt_MPF", 0.3);
  j2_maxAbsPt		= GetSettings(settings, "j2_maxAbsPt_MPF", 12000);

  min_delta_phi_balance = GetSettings(settings, "min_delta_phi_balance", 2.9);
  j2_maxRelPt_balance   = GetSettings(settings, "j2_maxRelPt_balance", 0.3);
  j2_maxAbsPt_balance   = GetSettings(settings, "j2_maxAbsPt_balance", 12000);

  minPt_jvtCut          = settings->GetValue("minPt_jvtCut", 60);
  eta_jvtCut            = settings->GetValue("eta_jvtCut", 2.4);
  jvtCut                = settings->GetValue("jvtCut", 0.59);
  jvtCutLoose 		= settings->GetValue("jvtCutLoose", 0.11);
  jvtCutTight		= settings->GetValue("jvtCutTight", 0.91);
  maxLeadingJetEta	= settings->GetValue("leadingJetEta", 0.8);

  METCollection         = configs->GetValue("METCollection", "");


  if (!(isPhoton or isElectron or isMuon)){
    Error("initialize()", "Must specify refence object (photon, electrons or muons)." );
  }
  if (   (isPhoton and (isElectron or isMuon))
      or (isElectron and (isPhoton or isMuon))
      or (isMuon and (isElectron or isPhoton))){
    Error("initialize()", "Must specify only one refence object (photon, electrons or muons)." );
  }

  // ////////////////////////////////////////
  // Getting trigger bins for gamma+jet.
  // Plan is to put comma separated 
  // startingPt-triggerName pairs in configs 
  // and have them separated and sorted here.
  // ////////////////////////////////////////
  string triggerString = settings->GetValue("triggerBins", "");
  string delimiter = ",";
  size_t pos = 0;
  size_t pos2, pos3;
  string token, token2;
  string triggerName, lowPt, highPt;
  while ((pos = triggerString.find(delimiter)) != string::npos) {
    token = triggerString.substr(0, pos);
    pos2 = token.find(":");
    if (pos2 == string::npos) Error("initialize()", "Trigger list in configuration file must follow 'TriggerName1:lowPt1-highPt1,TriggerName2:lowPt2-highPt2,' format");
    else{
      triggerName = token.substr(0,pos2);
      token2 = token.substr(pos2+1, token.length());
      pos3 = token2.find("-");  
     if (pos3 == string::npos) Error("initialize()", "Trigger list in configuration file must follow 'TriggerName1:lowPt1-highPt1,TriggerName2:lowPt2-highPt2,' format");
      else {
        lowPt  = token2.substr(0, pos3);
        highPt = token2.substr(pos3+1, token2.length());
      }
      triggerBins.push_back(make_pair(triggerName, make_pair(stod(lowPt.c_str()), stod(highPt.c_str()))));
    }
    triggerString.erase(0, pos + delimiter.length());
  }

  // ///////////////////
  // Initialize GRL tool
  // ///////////////////
  grl   = configs->GetValue("grl", "");
  m_grl = new GoodRunsListSelectionTool("GoodRunsListSelectionTool");
  std::vector<std::string> vecStringGRL;
  vecStringGRL.push_back(grl.c_str());
  m_grl->setProperty( "GoodRunsListVec", vecStringGRL).ignore();
  m_grl->setProperty("PassThrough", false).ignore(); 
  if (!m_grl->initialize().isSuccess()) { 
       Error("initialize()", "Failed to properly initialize the GRL. Exiting." );
  }
  vecStringGRL.clear();

  // ////////////////////
  // Initialize JES tools
  // ////////////////////

  // I'm making two JES tools as this is the easiest way I found to 
  // pull out the GSC calibration factor
  name = JetCollection.substr(0,JetCollection.find("Jets"));
  
  string configName     = "JES_MC15Prerecommendation_December2015_EtaIntercalOnly.config";
  string mainChainName  = "JetArea_Residual_Origin_EtaJES_GSC";
  string chainName      = "JetArea_Residual_Origin_EtaJES";

  isPFlow=false;
  if (name=="AntiKt4EMPFlow"){
    cout << "***************** No jet cleaning applied to PFlow right now **********************" << endl;
    isPFlow=true;
    configName    ="JES_MC15cRecommendation_PFlow_EtaIntercalOnly_March2017.config";
    mainChainName ="JetArea_Residual_EtaJES_GSC";
    chainName     ="JetArea_Residual_EtaJES";
  }
 
  m_JetCalibrationTool=new JetCalibrationTool("JetCalibrationTool");
  m_JetCalibrationTool->setProperty("JetCollection",name).ignore();
  m_JetCalibrationTool->setProperty("ConfigFile", configName).ignore();
  m_JetCalibrationTool->setProperty("CalibSequence", chainName).ignore();
  m_JetCalibrationTool->setProperty("IsData", not isMC).ignore();

  if (! m_JetCalibrationTool->initializeTool("JetCalibrationTool").isSuccess()) {
       Error("initialize()", "Failed to properly initialize the JetCalibrationTool. Exiting." );
  }

  m_GSCJetCalibrationTool = new JetCalibrationTool("JetCalibrationToolGSC");
  m_GSCJetCalibrationTool->setProperty("JetCollection", name).ignore();
  m_GSCJetCalibrationTool->setProperty("ConfigFile", configName).ignore();
  m_GSCJetCalibrationTool->setProperty("CalibSequence", mainChainName).ignore();
  m_GSCJetCalibrationTool->setProperty("IsData", not isMC).ignore();
//  m_GSCJetCalibrationTool->msg().setLevel( MSG::FATAL );
  if (! m_GSCJetCalibrationTool->initializeTool("JetCalibrationToolGSC").isSuccess()) {
       Error("initialize()", "Failed to properly initialize the JetCalibrationTool. Exiting." );
  }


  // ////////////////////////////////
  // Initialize the jet cleanin tool, 
  // which removes jets in hot spots
  // ////////////////////////////////
  m_jetCleaning = new JetCleaningTool("JetCleaning");
  m_jetCleaning->setProperty( "CutLevel", "LooseBad").ignore();
  m_jetCleaning->setProperty("DoUgly", false).ignore();
  if (! m_jetCleaning->initialize().isSuccess() ){
    Error("Init()", "Failed to properly initialize the JetCleaning Tool. Exiting." );
  }

  // //////////////////////////////////
  // Inittialize and configure the jvt
  // tool for rejection pileup jets
  // //////////////////////////////////
  ToolHandle<IJetUpdateJvt> hjvtagup;
  m_JVTtool = new JetVertexTaggerTool("jvtag");
  hjvtagup = ToolHandle<IJetUpdateJvt>("jvtag");
  bool fail = false;
  fail |= m_JVTtool->setProperty("JVTFileName","JetMomentTools/JVTlikelihood_20140805.root").isFailure();
  fail |= m_JVTtool->initialize().isFailure();
  if ( fail ) {
    Error("Init()", "Failed to properly initialize the JVT Tool. Exiting." );
  }

  // /////////////////
  // Muon Smearing tool
  // //////////////////
  if (isMuon or isElectron){
    if (isMuon) {
      Mode("Z->mumu");
    }
    // /////////////////////////////////////
    // Initialize muon smearing tool if in
    // electron mode to allow combination of
    // Z->ee and Z->mumu results
    // ///////////////////////////////////// 
    m_muonCalibrationAndSmearingTool = new CP::MuonCalibrationAndSmearingTool( "MuonCorrectionTool" );
    if (! m_muonCalibrationAndSmearingTool->initialize().isSuccess() ){
      Error("initialize()", "Failed to properly initialize the MuonCalibrationAndSmearingTool Tool. Exiting." );
    }
  }
  

  // ///////////////////////
  // Egamma calibration tool
  // ///////////////////////

  // /////////////////////////////////////
  // Initialize egamma smearing tool even 
  // if in muon mode to allow combination   
  // of Z->ee and Z->mumu results
  // ///////////////////////////////////// 
  m_EgammaCalibrationAndSmearingTool = new CP::EgammaCalibrationAndSmearingTool("EgammaCalSmear"); 
  m_EgammaCalibrationAndSmearingTool->setProperty("ESModel", "es2016PRE").ignore();  // see below for options
  m_EgammaCalibrationAndSmearingTool->setProperty("decorrelationModel",  "1NP_v1").ignore();  // see below for options  
//  m_EgammaCalibrationAndSmearingTool->setProperty("decorrelationModel",  "FULL_ETACORRELATED_v1").ignore();
  if (! m_EgammaCalibrationAndSmearingTool->initialize().isSuccess() ){
    Error("initialize()", "Failed to properly initialize the EgammaCalibrationAndSmearingTool Tool. Exiting." );
  }
  if (isElectron) {
    Mode("Z->ee mode");
  } else if (isPhoton) { 
    Mode("Gamma+jet mode");
    photons_shallowCopy.first = new xAOD::PhotonContainer;
    photons_shallowCopy.second = new xAOD::PhotonAuxContainer;
    photons_shallowCopy.first->setStore( photons_shallowCopy.second );
  }

  // ////////////////////////////
  // Initialize temporary storage
  // ////////////////////////////
  goodCalibJets = new xAOD::JetContainer; 
  goodCalibJetsAux = new xAOD::JetAuxContainer;
  goodCalibJets->setStore( goodCalibJetsAux ); 
  
  isolatedJets = new xAOD::JetContainer; 
  isolatedJetsAux = new xAOD::JetAuxContainer;
  isolatedJets->setStore( isolatedJetsAux ); 

  // ///////////////////////////////////////
  // Additional collections, only used to do
  // selection with varied JVT cuts
  // ///////////////////////////////////////
  isolatedJetsLooseJVT = new xAOD::JetContainer;
  isolatedJetsLooseJVTAux = new xAOD::JetAuxContainer;
  isolatedJetsLooseJVT->setStore( isolatedJetsLooseJVTAux );

  isolatedJetsTightJVT = new xAOD::JetContainer;
  isolatedJetsTightJVTAux = new xAOD::JetAuxContainer;
  isolatedJetsTightJVT->setStore( isolatedJetsTightJVTAux );


  // ///////////////////
  // Testing MET builder
  // ///////////////////
  metSystTool = new met::METSystematicsTool("METSystematicsTool");
  metSystTool->setProperty("JetColl"  ,JetCollection).ignore(); //this should be the same type as you give to rebuildJetMet

  metMaker = new met::METMaker("METMaker");
  if( (!metSystTool->initialize()) )
      {Error("initialize()", "Failed to properly initialize the MET tools. Exiting." );}
  metMaker->setProperty("JetSelection","Expert").ignore();
  metMaker->setProperty("CustomCentralJetPt", 9e12).ignore();
  metMaker->setProperty("CustomForwardJetPt", 9e12).ignore();
  metMaker->setProperty("DoMuonEloss", true).ignore();
//  metMaker->setProperty("DoIsolMuonEloss", true).ignore();
  if ((!metMaker->initialize()))
      {Error("initialize()", "Failed to properly initialize the MET tools. Exiting." );}

  doTracks=false;
  // ///////////
  // Systematics    
  // ///////////
  const CP::SystematicRegistry& registry = CP::SystematicRegistry::getInstance();
  const CP::SystematicSet& recommendedSystematics = registry.recommendedSystematics();
  for(CP::SystematicSet::const_iterator sysItr = recommendedSystematics.begin(); sysItr != recommendedSystematics.end(); ++sysItr) {
    if (isPhoton and TString(sysItr->name()).Contains("EL")) continue;
    if ((isMuon or isElectron) and TString(sysItr->name()).Contains("PH")) continue;
    if (TString(sysItr->name()).Contains("MET")) continue;
    sysList.push_back(CP::SystematicSet());
    sysList.back().insert(*sysItr);
  }
  sysList.push_back(CP::SystematicSet("NoSyst"));



  // ///////////////
  // Prepare cutflow
  // ///////////////
  Cuts.clear();
  AddCut("All Passed");
  AddCut("GRL");
  AddCut("LAr Error");
  AddCut("Vertex Cut");
  AddCut("Trigger");
  if (isPhoton) {
    AddCut("At least one photon");
    AddCut("Binned trigger");
    AddCut("Photon: author");
    AddCut("Photon: tight");
    AddCut("Photon: ambiguity resolver");
    AddCut("Photon: quality");
    AddCut("Photon: cleaning");
    AddCut("Photon: Pt");
    AddCut("Photon: crack");
    AddCut("Photon: Eta");
    AddCut("Photon: Isolation");
    AddCut("Photon: Conversion");
    AddCut("Photon: Brem");
  } else {

    AddCut("2 leptons");
    AddCut("Charge");
    AddCut("Z Mass");
    AddCut("Z Pt");
  }
  AddCut("Jets after overlap removal");
  AddCut("nGoodJets");
  AddCut("Lead jet: Pt");
  AddCut("Lead jet: eta");
  AddCut("dPhi");
  AddCut("Veto");


  MPFSyst = new std::vector<pair<TString, TLorentzVector>>;
  BalSyst = new std::vector<pair<TString, TLorentzVector>>;
  Syst    = new std::vector<pair<TString, TLorentzVector>>;

  objectFilter=new ObjectFilter();
  objectFilter->Init(selectionFile2, &Cuts, isMC);

  delete configs;
  delete settings;
}

// ---------------------------------------------------------
bool EventFilter::PassVJet(xAOD::TEvent *m_event)
// ////////////////////////////////////////////////////
// Returns false if event egregious fails some cut. 
// If event fails nominal selection but passes looser
// version of the cut returns true with CutMask and 
// BalanceCutMask containing lists of passed and failed
// cuts. 
// ////////////////////////////////////////////////////
{
  fill(CutMask.begin(), CutMask.end(), true);
  fill(BalanceCutMask.begin(), BalanceCutMask.end(), true);
  RelSubleadingJetPt=0;
  dPhi=0;
  objectFilter->IsolE = -100;
  objectFilter->isTight = -2;
  corrX = corrY = 0;
  METUtilx = METUtily = 0;
  subleadingJetEta = -6;
  Ref.SetPxPyPzE(0,0,0,0);
  TrueJetPt=0;
  TrueMF=0;

  // ////////////////////////////////
  // Create a transient object store.
  // Needed for the CP tools.       
  // ////////////////////////////////
  xAOD::TStore store; 

  const xAOD::MissingETContainer* coreMet  = nullptr;
  std::string coreMetKey = "MET_Core_AntiKt4EMTopo";
  std::string metAssocKey = "METAssoc_AntiKt4EMTopo";
  if (JetCollection=="AntiKt4LCTopoJets"){
    coreMetKey = "MET_Core_AntiKt4LCTopo";
    metAssocKey = "METAssoc_AntiKt4LCTopo";
  }
  if (JetCollection=="AntiKt4EMPFlowJets"){
    coreMetKey = "MET_Core_AntiKt4EMPFlow";
    metAssocKey = "METAssoc_AntiKt4EMPFlow";
  }
    
  assert( metSystTool->evtStore()->retrieve(coreMet, coreMetKey) );
  

  assert( metSystTool->evtStore()->retrieve(metMap, metAssocKey) );
  // Create a MissingETContainer with its aux store for each systematic
  newMetContainer    = new xAOD::MissingETContainer();
  xAOD::MissingETAuxContainer* newMetAuxContainer = new xAOD::MissingETAuxContainer();
  newMetContainer->setStore(newMetAuxContainer);
  // It is necessary to reset the selected objects before every MET calculation
  metMap->resetObjSelectionFlags();


  MPFSyst->clear();
  BalSyst->clear();
  Syst->clear();
  MPF_FirstFailed = FindCode("All Passed");
  Bal_FirstFailed = FindCode("All Passed");


  if (!EventLevel(m_event)) {  
    delete newMetAuxContainer;
    delete newMetContainer;
    newMetContainer=0;
    return false;
  }

  bool pass=true;
  if (isPhoton){
    if (!PassPhoton(m_event)){
      pass=false;
    }
  } else if (isElectron) {
    if (!PassElectron(m_event)){
      pass=false;
    }
  } else if (isMuon) {
    if (!PassMuon(m_event)){
      pass=false;
    }
  }

  if(not pass){
    delete newMetAuxContainer;
    delete newMetContainer;
    newMetContainer=0;
    return false;
  }

// More leftovers from testing the MET builder
  const xAOD::JetContainer* calibJets = nullptr;
  assert( metSystTool->evtStore()->retrieve(calibJets, JetCollection)); 



  string softTerm = "SoftClus";
  if (JetCollection=="AntiKt4EMTopoJets") softTerm="SoftClusEM";

  assert(  metMaker->rebuildJetMET("RefJet",        //name of jet met
                                   softTerm,      //name of soft cluster term met
                                   "PVSoftTrk",       //name of soft track term met
                                   newMetContainer,  //adding to this new met container
                                   calibJets,        //using this jet collection to calculate jet met
                                   coreMet,          //core met container
                                   metMap,           //with this association map
                                   false            //don't apply jet jvt cut
                                    )
        );

  xAOD::MissingET *softClusMet = (*newMetContainer)[softTerm];
  
  METUtilx = softClusMet->mpx();
  METUtily = softClusMet->mpy();
 
  if(isMuon){
    xAOD::MissingET *MuonMet     = (*newMetContainer)["RefMuon"];
    METUtilx+= MuonMet->mpx();
    METUtily+= MuonMet->mpy();
    xAOD::MissingET *MuonMissingMET = (*newMetContainer)["MuonEloss"];
    METUtilx+=MuonMissingMET->mpx();
    METUtily+=MuonMissingMET->mpy();
  }
  if(isElectron){
    xAOD::MissingET *ElectronMet     = (*newMetContainer)["RefEle"];
    METUtilx+= ElectronMet->mpx();
    METUtily+= ElectronMet->mpy();
    //cout << ElectronMet->mpy() << endl;
  }

  if(isPhoton){
    xAOD::MissingET *PhotonMet   = (*newMetContainer)["RefPhoton"];
    METUtilx+=PhotonMet->mpx();
    METUtily+=PhotonMet->mpy();
  }

  delete newMetAuxContainer;
  delete newMetContainer;
  newMetContainer=0;


  if (!CheckJets(m_event)){
    return false;
  }
  return true;
}

// ---------------------------------------------------------
bool EventFilter::EventLevel(xAOD::TEvent *m_event)
// ////////////////////////////////////////////////////
// As the name suggests this is for general event level 
// event selection criteria, like making sure the event
// is on the grl and other cleaning requirements. 
// /////////////////////////////////////////////////////
{
  // ///////////////////////////////
  // Getting event level information
  // from the xAOD                   
  // ///////////////////////////////
  eventInfo = 0;
  if( ! m_event->retrieve( eventInfo, "EventInfo").isSuccess() ){
     Error("execute()", "Failed to retrieve event info collection. Exiting." );
     return false;
  }
  // ///////////////////////////////////
  // Checking if the event is data or MC
  // ///////////////////////////////////
  if(eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){
     if (not isMC) {
       Error("execute()", "isMC in eventinfo and in the config file do not match" );
       return false;
    }
  }
  else {
    if (isMC) {
       Error("execute()", "isMC in eventinfo and in the config file do not match" );
       return false;
    }
  }

  // /////////////////////////////////
  // Check the good runs list for data
  // /////////////////////////////////
  if(!isMC and doGRL){
    if(!m_grl->passRunLB(*eventInfo)){
      Fail("GRL");
      return false;
    }
  }

  // ////////////////////////////////////
  // Remove events with detector problems 
  // in the LAr, tilecal, etc.
  // ////////////////////////////////////
  if(!isMC){
    if((eventInfo->errorState(xAOD::EventInfo::LAr)==xAOD::EventInfo::Error)||(eventInfo->errorState(xAOD::EventInfo::Tile)==xAOD::EventInfo::Error )||(eventInfo->isEventFlagBitSet(xAOD::EventInfo::Core,18))){
      Fail("LAr Error");
      return false; 
    } 
  }

  // ///////////////////////////////////////
  // Primary vertex minimum track requiremnt
  // Load vertex container                   
  // /////////////////////////////////////// 
  pVertices = 0;
  if( !m_event->retrieve(pVertices,"PrimaryVertices") ){
    Error("execute()", "Failed to retrieve PrimaryVertices. Exiting." );
    return false; 
  }

  // //////////////////////////////////
  // Assume first vertex is correct one
  // //////////////////////////////////
  pVertex = 0;
  if( pVertices->size() > 0 ){
    pVertex = (*pVertices)[0];
  } else {
    Fail("Vertex Cut");
    return false;
  }
  if(pVertex->nTrackParticles() < 2){
    Fail("Vertex Cut");
    return false; 
  } 


  // //////////////////////////////
  // Double checking primary vertex 
  // //////////////////////////////
  NPV=0;
  iVertex = -1;
  float pt2PV = 0;
  xAOD::VertexContainer::const_iterator vtxItr = pVertices->begin();
  xAOD::VertexContainer::const_iterator vtxItrE = pVertices->end();
  int ivtx = -1;
  for(; vtxItr!=vtxItrE; ++vtxItr){
    ++ivtx;
    const std::vector< ElementLink<xAOD::TrackParticleContainer> > vxTracks = (*vtxItr)->trackParticleLinks();
    float ptsum = 0;
    if (vxTracks.size()>=2) NPV++;
    for(unsigned int it=0; it<vxTracks.size(); ++it){
      if( (vxTracks[it]).isValid() ){
        const xAOD::TrackParticle* trkit = *(vxTracks[it]);
        if( trkit ) ptsum += trkit->p4().Perp2();
      }
    }
    if( ptsum > pt2PV ){
      pt2PV = ptsum;
      iVertex = ivtx;
    }
  }
  pVertices = 0;

  return true;
}

// ---------------------------------------------------------
bool EventFilter::CheckJets(xAOD::TEvent *m_event)
// ///////////////////////////////////////////////////////////////////
// To be done after the reference selection.  This function checks
// that the jet passes all selection criteria (JVT, quality, etc.)
// and then confirms that the jets pass more global criteria, 
// like the jet and reference are back to back and that the subleading
// jet has a low Pt relative to the reference object. 
// ////////////////////////////////////////////////////////////////////
{
  // //////////////////
  // Get jet collection
  // //////////////////
  const xAOD::JetContainer* jets = 0;
  if ( !m_event->retrieve( jets, JetCollection ).isSuccess() ){ 
    Error("execute()", "Failed to retrieve Jet container. Exiting." );
    return false;
  }

  xAOD::JetContainer::const_iterator jet_itr = jets->begin();
  xAOD::JetContainer::const_iterator jet_end = jets->end();

  if (jet_itr == jet_end) {
    Fail("nGoodJets");
    return false;
  }


  // //////////////////////////////////// 
  // Applies JES and checks jet quality.
  //  - Currently just jet cleaning tool. 
  // ////////////////////////////////////
  goodCalibJets->clear();
  for( ; jet_itr != jet_end; ++jet_itr ){
    if( (m_jetCleaning->accept( **jet_itr ))){
      xAOD::JetFourMom_t p4aodraw = (*jet_itr)->jetP4(xAOD::JetScale::JetConstitScaleMomentum);
      xAOD::Jet* calibJet = 0;
      m_JetCalibrationTool->calibratedCopy(**jet_itr, calibJet);
      xAOD::JetFourMom_t p4aodCal = (calibJet)->jetP4();
      delete calibJet;
      xAOD::Jet* calibGSCJet = 0;
      m_GSCJetCalibrationTool->calibratedCopy(**jet_itr, calibGSCJet).ignore(); 
      calibGSCJet->setJetP4("xaodscale",p4aodraw);
      calibGSCJet->setJetP4("JES",p4aodCal);
      goodCalibJets->push_back(calibGSCJet);
    }
    else if (not isPFlow){
      xAOD::Jet* calibJet = 0;
      m_JetCalibrationTool->calibratedCopy(**jet_itr, calibJet);
      if (calibJet->pt() > 20*GeV){
        delete calibJet;
        Fail("nGoodJets");
        return false;
      }
      delete calibJet;
    } 
  }



  if (goodCalibJets->begin() == goodCalibJets->end()){
    Fail("nGoodJets");
    return false;
  }

  // ///////////////////////////////////////////////////
  // Remove photons or electrons from the jet collection
  // Checks JVT
  // ///////////////////////////////////////////////////
  isolatedJets->clear();
  isolatedJetsLooseJVT->clear();
  isolatedJetsTightJVT->clear();
  for( jet_itr = goodCalibJets->begin(); jet_itr != goodCalibJets->end(); ++jet_itr ){
    bool isIsol = true;
    if (isPhoton and ( (*jet_itr)->p4().DeltaR(Ref) < ph_overlapRemoval)) isIsol = false;
    else if ((*jet_itr)->p4().DeltaR(Lep1) < lep_overlapRemoval or (*jet_itr)->p4().DeltaR(Lep2) < lep_overlapRemoval) isIsol = false;
    if (isIsol){
        float JVT = m_JVTtool->updateJvt((**jet_itr)); 
        if (fabs((*jet_itr)->p4().Eta()) < eta_jvtCut and (*jet_itr)->p4().Pt()/GeV < minPt_jvtCut){
          if ( JVT > jvtCut){
            newJet = new xAOD::Jet;
            newJet->makePrivateStore(**jet_itr);
            isolatedJets->push_back(newJet);
          }
          if ( JVT > jvtCutLoose){
            newJet = new xAOD::Jet;
            newJet->makePrivateStore(**jet_itr);
            isolatedJetsLooseJVT->push_back(newJet);
          }
          if ( JVT > jvtCutTight){
            newJet = new xAOD::Jet;
            newJet->makePrivateStore(**jet_itr);
            isolatedJetsTightJVT->push_back(newJet);
          }
        }
       else if (fabs((*jet_itr)->p4().Eta()) < 4.5){ 
         newJet = new xAOD::Jet;
         newJet->makePrivateStore(**jet_itr);
         isolatedJets->push_back(newJet);
         newJet = new xAOD::Jet;
         newJet->makePrivateStore(**jet_itr);
         isolatedJetsLooseJVT->push_back(newJet);
         newJet = new xAOD::Jet;
         newJet->makePrivateStore(**jet_itr);
         isolatedJetsTightJVT->push_back(newJet);
       }
    }
  }

  // ///////////////////////////////////////////
  // Finish the selection for the JVT variations
  // ///////////////////////////////////////////
  bool pass = true;
  for( vector<bool>::const_iterator i = CutMask.begin(); i != CutMask.end(); ++i){
    pass = pass and (*i);
  }
  if (pass){
    if(isolatedJetsLooseJVT->begin()!=isolatedJetsLooseJVT->end()){
      jet_itr = isolatedJetsLooseJVT->begin();
      if((*jet_itr)->pt()/GeV>leadingJet_MinPt){
        if(fabs((*jet_itr)->eta())<maxLeadingJetEta){
          leadingJet = (*jet_itr)->p4();
          jet_itr++;
          double subleadingJetPt = 0;
          if (jet_itr != isolatedJetsLooseJVT->end()) {
            subleadingJetPt  = (*jet_itr)->pt()/GeV;
          }
          double RefPt = Ref.Pt()/GeV;
          RelSubleadingJetPt = subleadingJetPt / RefPt;
          dPhi = fabs(leadingJet.DeltaPhi(Ref));
          if (fabs(leadingJet.DeltaPhi(Ref)) > min_delta_phi[nominal]) {
            double maxJ2Pt = j2_maxAbsPt[nominal];
            if (j2_maxRelPt[nominal]*RefPt > maxJ2Pt) maxJ2Pt = j2_maxRelPt[nominal]*RefPt;
            if (subleadingJetPt < maxJ2Pt){
              Syst->push_back(make_pair(TString("JVT__1down"), Ref));
            }
          }
        }
      } 
    }
    if(isolatedJetsTightJVT->begin()!=isolatedJetsTightJVT->end()){
      jet_itr = isolatedJetsTightJVT->begin();
      if((*jet_itr)->pt()/GeV>leadingJet_MinPt){
        if(fabs((*jet_itr)->eta())<maxLeadingJetEta){
          leadingJet = (*jet_itr)->p4();
          jet_itr++;
          double subleadingJetPt = 0;
          if (jet_itr != isolatedJetsTightJVT->end()) {
            subleadingJetPt  = (*jet_itr)->pt()/GeV;
          }
          double RefPt = Ref.Pt()/GeV;
          RelSubleadingJetPt = subleadingJetPt / RefPt;
          if (fabs(leadingJet.DeltaPhi(Ref)) > min_delta_phi[nominal]) {
            double maxJ2Pt = j2_maxAbsPt[nominal];
            if (j2_maxRelPt[nominal]*RefPt > maxJ2Pt) maxJ2Pt = j2_maxRelPt[nominal]*RefPt;
            if (subleadingJetPt < maxJ2Pt){
              Syst->push_back(make_pair(TString("JVT__1up"), Ref));
            }
          }
        }
      }
    }
  }

  // ///////////////////////////////////////
  // Make sure that we have at least one jet
  // ///////////////////////////////////////
  if (isolatedJets->begin() == isolatedJets->end()){
    Fail("Jets after overlap removal");
    return false;
  }

  // //////////////////////////////////////////
  // Selecting leading jets
  //
  // Also getting the leading jet Pt with and 
  // without GSC to get the GSC scaelfactor and
  // the truth jet Pt just to look at the truth
  // response. 
  // //////////////////////////////////////////
  jet_itr = isolatedJets->begin();
  jet_end = isolatedJets->end();
  leadingJetGSC = (*jet_itr)->pt() / (*jet_itr)->jetP4("JES").pt();
  leadingJetJES = (*jet_itr)->jetP4("JES").pt()/ (*jet_itr)->jetP4("xaodscale").pt();

  if(isMC){
    TrueJetPt=(*jet_itr)->getAttribute<double>("GhostTruthPt");
    TrueMF=(*jet_itr)->getAttribute<double>("GhostTruthAssociationFraction");
  }

  double PassJetEta=true;
  if ((*jet_itr)->pt()/GeV < leadingJet_MinPt){
    Fail("Lead jet: Pt");
  }
  
  if (fabs((*jet_itr)->eta()) > maxLeadingJetEta){
    PassJetEta=false;
    Fail("Lead jet: eta");
  }


  // //////////////////////////////////
  // Keeping nominal calibrated jet, 
  // as well as const and const-pileup
  // scale for additional studies. 
  // //////////////////////////////////
  leadingJet = (*jet_itr)->p4(); 
  leadingJetConst.SetPtEtaPhiM((*jet_itr)->jetP4("xaodscale").pt(), 
                               (*jet_itr)->jetP4("xaodscale").eta(), 
                               (*jet_itr)->jetP4("xaodscale").phi(),  
                               (*jet_itr)->jetP4("xaodscale").M());

  leadingJetPileupCorr.SetPtEtaPhiM((*jet_itr)->jetP4("JetPileupScaleMomentum").pt(), 
                                    (*jet_itr)->jetP4("JetPileupScaleMomentum").eta(), 
                                    (*jet_itr)->jetP4("JetPileupScaleMomentum").phi(), 
                                    (*jet_itr)->jetP4("JetPileupScaleMomentum").M());
 
 /* cout << "dumping jet" << endl; 
  for(size_t i=0;i<(*jet_itr)->numConstituents();++i){
    if ((*jet_itr)->rawConstituent(i)!=NULL){ // I'm finding sometimes these pointers don't actually point to anything, causing crashes
     // Check that it is in fact a cluster:
      if( (*jet_itr)->rawConstituent(i)->type() != xAOD::Type::CaloCluster ) {
        cout << "ERROR, NON CLUSTER CONSTITUENT TO RECO JET" << endl;
        return false;
      }
      const xAOD::CaloCluster* clus = dynamic_cast< const xAOD::CaloCluster* >((*jet_itr)->rawConstituent(i));
      cout << clus->rawE() << endl;
    }
  }
  */


  deltaRnearestJet=999;
  jet_itr++;
  for( ; jet_itr != isolatedJets->end(); ++jet_itr ){ 
    if ( (*jet_itr)->p4().DeltaR(leadingJet)<deltaRnearestJet) deltaRnearestJet=(*jet_itr)->p4().DeltaR(leadingJet);
  }
  jet_itr = isolatedJets->begin();
  jet_itr++;
  double subleadingJetPt = 0;
  if (jet_itr != isolatedJets->end()) {
    subleadingJet=(*jet_itr)->p4();
    subleadingJetPt  = (*jet_itr)->pt()/GeV; 
    subleadingJetEta =(*jet_itr)->eta();
  }
  double RefPt = Ref.Pt()/GeV;
  RelSubleadingJetPt = subleadingJetPt / RefPt;
  dPhi = fabs(leadingJet.DeltaPhi(Ref));

  // ///////////////////////////////////
  //  Egamma variation systs
  //
  // Need to make sure that the event
  // passed radiation cuts with the 
  // varied reference object.  
  // Probably only really need to check
  // J2, and even then I doubt this 
  // matters much but better safe than 
  // sorry. 
  // ///////////////////////////////////
  vector<pair<TString, TLorentzVector>>::const_iterator it = Syst->begin();
  vector<pair<TString, TLorentzVector>>::const_iterator it_end = Syst->end();
  for(;it!=it_end;++it) {
    double maxJ2PtMPF = j2_maxAbsPt[nominal];
    if (j2_maxRelPt[nominal]*RefPt > maxJ2PtMPF) maxJ2PtMPF = j2_maxRelPt[nominal]*RefPt;
    bool passdPhiMPF = fabs(leadingJet.DeltaPhi((*it).second)) >= min_delta_phi[nominal];
    bool passJ2MPF   = subleadingJetPt <= maxJ2PtMPF;
    if (passJ2MPF and passdPhiMPF and PassJetEta) {
      MPFSyst->push_back(*it);
    }
    double maxJ2PtBal = j2_maxAbsPt_balance[nominal];
    if (j2_maxRelPt_balance[nominal]*RefPt > maxJ2PtBal) maxJ2PtBal = j2_maxRelPt_balance[nominal]*RefPt;
    bool passdPhiBal = fabs(leadingJet.DeltaPhi((*it).second)) >= min_delta_phi_balance[nominal];
    bool passJ2Bal   = subleadingJetPt <= maxJ2PtBal;
    if (passJ2Bal and passdPhiBal and PassJetEta) {
	BalSyst->push_back(*it);
    }
  }

  // /////////////////////////////////////////////////////
  // Checking variations of the radiation supressing cuts.
  // /////////////////////////////////////////////////////
  pass = true;
  for( vector<bool>::const_iterator i = CutMask.begin(); i != CutMask.end(); ++i){
    pass = pass and (*i);
  }
  if (pass){
    double nom_maxJ2Pt_mpf = j2_maxAbsPt[nominal];
    if (j2_maxRelPt[nominal]*RefPt > nom_maxJ2Pt_mpf) nom_maxJ2Pt_mpf = j2_maxRelPt[nominal]*RefPt;
    for (int i = tight; i != loose+1; ++i){
      if (subleadingJetPt <= nom_maxJ2Pt_mpf){
        if (fabs(leadingJet.DeltaPhi(Ref)) >= min_delta_phi[i]){
          if (i == loose)   MPFSyst->push_back(make_pair(TString("dPhi__1down"), Ref));
          if (i == tight)   MPFSyst->push_back(make_pair(TString("dPhi__1up"), Ref)); 
        }
      }
      if (fabs(leadingJet.DeltaPhi(Ref)) >= min_delta_phi[nominal]){
        double maxJ2Pt = j2_maxAbsPt[i];
        if (j2_maxRelPt[i]*RefPt > maxJ2Pt) maxJ2Pt = j2_maxRelPt[i]*RefPt;
        if (subleadingJetPt <= maxJ2Pt){ 
          if (i == loose)   MPFSyst->push_back(make_pair(TString("Veto__1down"), Ref));
          if (i == tight)   MPFSyst->push_back(make_pair(TString("Veto__1up"), Ref));
        }
      }
    }
    double nom_maxJ2Pt_bal = j2_maxAbsPt_balance[nominal];
    if (j2_maxRelPt_balance[nominal]*RefPt > nom_maxJ2Pt_bal) nom_maxJ2Pt_bal = j2_maxRelPt_balance[nominal]*RefPt;
    for (int i = tight; i != loose+1; ++i){
      if (subleadingJetPt <= nom_maxJ2Pt_bal){
        if (fabs(leadingJet.DeltaPhi(Ref)) >= min_delta_phi_balance[i]){
          if (i == loose)   BalSyst->push_back(std::make_pair(TString("dPhi__1down"), Ref));
          if (i == tight)   BalSyst->push_back(std::make_pair(TString("dPhi__1up"), Ref));
        }
      }
      if (fabs(leadingJet.DeltaPhi(Ref)) >= min_delta_phi_balance[nominal]){
        double maxJ2Pt = j2_maxAbsPt_balance[i];
        if (j2_maxRelPt_balance[i]*RefPt > maxJ2Pt) maxJ2Pt = j2_maxRelPt_balance[i]*RefPt ;
//        if (i == loose) maxJ2Pt += 0.1 * RefPt;
        if (subleadingJetPt <= maxJ2Pt){            
          if (i == loose)   BalSyst->push_back(std::make_pair(TString("Veto__1down"), Ref));
          if (i == tight)   BalSyst->push_back(std::make_pair(TString("Veto__1up"), Ref));
        }
      }
    }
  }

  // //////////////////////
  // Nominal radiation cuts
  // //////////////////////

  // ///
  // MPF
  // ///
  if (fabs(leadingJet.DeltaPhi(Ref)) < min_delta_phi[nominal]) {
    CutMask[FindCode("dPhi")] = false;
    if (MPF_FirstFailed == FindCode("All Passed")) MPF_FirstFailed = FindCode("dPhi");
  }
  double maxJ2Pt = j2_maxAbsPt[nominal];
  if (j2_maxRelPt[nominal]*RefPt > maxJ2Pt) maxJ2Pt = j2_maxRelPt[nominal]*RefPt;
  if (subleadingJetPt > maxJ2Pt){
    CutMask[FindCode("Veto")] = false;
    if (MPF_FirstFailed == FindCode("All Passed")) MPF_FirstFailed = FindCode("Veto");
  }

  // //////////
  // Pt Balance
  // //////////  
  if (fabs(leadingJet.DeltaPhi(Ref)) < min_delta_phi_balance[nominal]) {
    BalanceCutMask[FindCode("dPhi")] = false;
    if (Bal_FirstFailed == FindCode("All Passed")) Bal_FirstFailed = FindCode("dPhi");
  }
  maxJ2Pt = j2_maxAbsPt_balance[nominal];
  if (j2_maxRelPt_balance[nominal]*RefPt > maxJ2Pt) maxJ2Pt = j2_maxRelPt_balance[nominal]*RefPt;
  if (subleadingJetPt > maxJ2Pt){
    BalanceCutMask[FindCode("Veto")] = false;
    if (Bal_FirstFailed == FindCode("All Passed")) Bal_FirstFailed = FindCode("Veto");
  }
  return true;
}

// ---------------------------------------------------------
bool EventFilter::PassPhoton(xAOD::TEvent *m_event)
// /////////////////////////////////////////////////
// Make sure a photon exists and also checks that it 
// passes quality cuts.  Also filled a vector with 
// variation names paired with TLorenz vectors of 
// the varied photon. 
// /////////////////////////////////////////////////
{
  //HardScat = false;
  // /////////////////////
  // Get photon collection
  // /////////////////////
  const xAOD::PhotonContainer* photons = 0;
  if ( !m_event->retrieve( photons, "Photons" ).isSuccess() ){ 
    Error("execute()", "Failed to retrieve PhotonCollection container. Exiting." );
    Fail("At least one photon");
    return false;
  }

  xAOD::PhotonContainer::const_iterator ph_itr = photons->begin();
  xAOD::PhotonContainer::const_iterator ph_end = photons->end();
  // /////////////////////////////
  // At least one photon is needed
  // /////////////////////////////
  if (ph_itr == ph_end){
    Fail("At least one photon");
    return false;
  }


  photons_shallowCopy.first->clear();
  for(ph_itr = photons->begin(); ph_itr != ph_end; ++ph_itr ){
    newPhoton = new xAOD::Photon;
    newPhoton->makePrivateStore(**ph_itr);
    photons_shallowCopy.first->push_back(newPhoton);
  }

  bool isPassed = false;

  xAOD::PhotonContainer* phsCorr = photons_shallowCopy.first;
  xAOD::PhotonContainer::iterator ph_it_last = phsCorr->end();
  vector<CP::SystematicSet>::const_iterator sysListItr;

  xAOD::PhotonContainer::iterator ph_it = phsCorr->begin();
  UncalLep1 = (*ph_it)->p4();

  vector<int> Cuts;
  if ((*ph_it)->author() !=128){
    if(!m_EgammaCalibrationAndSmearingTool->applyCorrection(**ph_it)) Error("PassPhoton()", "Cannot calibrate photon");
    }
 

  // //////////////////////////////////////////
  // PassPhoton here checks the photon author, 
  // Pt, eta, ID, isolation, etc.  Only checks
  // if photon is loose (loose not tight needed
  // for purity measurement. 
  // Cuts gives a pass/fail list of the cuts 
  // applied so a problematic cut can easily be
  // tracked down. 
  // //////////////////////////////////////////
  if(objectFilter->PassPhoton((*ph_it), &Cuts, true)){
    Ref = (*ph_it)->p4();
    isPassed = true;
  }

  // /////////////////////////////////////////////////////////
  // Making it simpler to check that the photon is tight later
  // /////////////////////////////////////////////////////////
  passTight = true;
  for(vector<int>::iterator it = Cuts.begin(); it != Cuts.end(); ++it){
    if ( (*(it) ==FindCode("Photon: tight"))) passTight = false; 
    Fail(*it);
  }
  isTight = objectFilter->isTight;
  Lep1 = Ref;
  Lep2 = Lep1;

  ConstDataVector<xAOD::PhotonContainer> metPhotons(SG::VIEW_ELEMENTS);
  metPhotons.push_back( (*photons->begin()));



  assert(metMaker->rebuildMET("RefPhoton",
                              xAOD::Type::Photon,
                              newMetContainer,
                              metPhotons.asDataVector(),
                              metMap, 
                              MissingETBase::UsageHandler::PhysicsObject) //OnlyCluster) //PhysicsObject)   // I'm not sure what should go here and I haven't managed to track down any documentation yet.  
          );


  // ////////////////////////////////////////////////////////
  // Same as above but redone for every systematic variation. 
  // ////////////////////////////////////////////////////////

  for (sysListItr = sysList.begin(); sysListItr != sysList.end(); ++sysListItr){
    TLorentzVector ScaleRef;
    // Tell the calibration tool which variation to apply
    if (m_EgammaCalibrationAndSmearingTool->applySystematicVariation(*sysListItr) != CP::SystematicCode::Ok) Error("PassPhoton()", "Cannot configure calibration tool for systematics");
    vector<int> Cuts;
    if ((*ph_it)->author() !=128){
      if(!m_EgammaCalibrationAndSmearingTool->applyCorrection(**ph_it)) Error("PassPhoton()", "Cannot calibrate photon");
      }
    if (sysListItr->name().find("MUON") != string::npos) continue;
    if(objectFilter->PassPhoton((*ph_it), &Cuts, false)){
      //newPhoton = *ph_it;
      ScaleRef = (*ph_it)->p4();
      bool test=true;
      if (Cuts.begin() == Cuts.end()) {
        test=false;
        Syst->push_back(make_pair(TString(sysListItr->name()), ScaleRef));
      }
      else {for (auto it=Cuts.begin();it!=Cuts.end();it++)if (!((*it)==FindCode("Photon: tight") and passTight)) test=false;}
      if (test)Syst->push_back(make_pair(TString(sysListItr->name()), ScaleRef));
      if ( TString(sysListItr->name()) == "EG_SCALE_NOMINAL") {
        Ref = ScaleRef;
        for(vector<int>::iterator it = Cuts.begin(); it != Cuts.end(); ++it){
          if (!((*it)==FindCode("Photon: tight") and passTight)) Fail(*it);
        }
      }
    } 
    else if ( TString(sysListItr->name()) == "EG_SCALE_NOMINAL") {
      for(vector<int>::iterator it = Cuts.begin(); it != Cuts.end(); ++it){ 
        Fail(*it);
      }
    }
  }

  return isPassed;
}


// ---------------------------------------------------------
bool EventFilter::PassElectron(xAOD::TEvent *m_event)
// /////////////////////////////////////////////////
// Checks to make sure exactly two electrons pass the 
// required quality/kinematic cuts and that they 
// reconstruct to something that looks like a Z boson.  
// Also filled a vector with 
// variation names paired with TLorenz vectors of 
// the varied Z. 
// /////////////////////////////////////////////////
{
  // Get electron collection
  const xAOD::ElectronContainer* electrons = 0;
  if ( !m_event->retrieve( electrons, "Electrons" ).isSuccess() ){ 
    Error("execute()", "Failed to retrieve Electrons container. Exiting." );
    Fail("2 leptons");
    return false;
  }


  ConstDataVector<xAOD::ElectronContainer> metElectrons(SG::VIEW_ELEMENTS);


  pair< xAOD::ElectronContainer*, xAOD::ShallowAuxContainer* > electrons_shallowCopy = xAOD::shallowCopyContainer( *electrons );
  xAOD::ElectronContainer::iterator electronsSC_itr = (electrons_shallowCopy.first)->begin();
  xAOD::ElectronContainer::iterator electronsSC_end = (electrons_shallowCopy.first)->end();
  xAOD::ElectronContainer::const_iterator  electrons_itr=electrons->begin();


  // //////////////////////////////
  // Quick check to make sure
  // there's at least one 
  // lepton, check to make sure 
  // there's two good ones is later
  // //////////////////////////////
  if (electronsSC_itr == electronsSC_end){
    Fail("2 leptons");
    return false;
  }

  int count = 0;
  int charge1 = 0;
  int charge2 = 0;


  // //////////////////////////////////////////
  // Run over all electrons in the event, first
  // calibrating each electron and then using 
  // PassElectron to check electron quality, 
  // Pt, eta, and isolation. 
  // //////////////////////////////////////////
  for( ; electronsSC_itr != electronsSC_end; ++electronsSC_itr, ++electrons_itr ) {
    beforeCal = (*electronsSC_itr)->p4();
    if(!m_EgammaCalibrationAndSmearingTool->applyCorrection(**electronsSC_itr)) Error("PassElectron()", "Cannot calibrate electron");
    if (objectFilter->PassElectron((*electronsSC_itr))){
      metElectrons.push_back((*electrons_itr));
      count += 1;
      Lep2 = Lep1;
      UncalLep2 = UncalLep1;
      charge2 = charge1;
      Lep1 = (*electronsSC_itr)->p4();
      UncalLep1 = beforeCal;
      charge1 = (*electronsSC_itr)->charge();
    }
  }

  // //////////////////////////////////////////////////////////
  // Make sure exactly two electrons passed the above selection
  // //////////////////////////////////////////////////////////
  if (count == 2) {
    if (charge1 * charge2 < 0){
      Ref = Lep1 + Lep2;
      if (!( Ref.M() > Z_Min and Ref.M() < Z_Max)) Fail("Z Mass");
    }
    else Fail("Charge");
  }



  assert(metMaker->rebuildMET("RefEle",
                              xAOD::Type::Electron,
                              newMetContainer,
                              metElectrons.asDataVector(),
                              metMap, MissingETBase::UsageHandler::PhysicsObject)  // I'm not sure what should go here and I haven't managed to track down any documentation yet.  
                            );


  std::vector<CP::SystematicSet>::const_iterator sysListItr;
  for (sysListItr = sysList.begin(); sysListItr != sysList.end(); ++sysListItr){
    int ScaleCount = 0;
    // Tell the calibration tool which variation to apply 
    if (m_EgammaCalibrationAndSmearingTool->applySystematicVariation(*sysListItr) != CP::SystematicCode::Ok) Error("PassElectron()", "Cannot configure calibration tool for systematics");
    int numEl = 0;
    electronsSC_itr = (electrons_shallowCopy.first)->begin();

    for( ; electronsSC_itr != electronsSC_end; ++electronsSC_itr ) {
      if(!m_EgammaCalibrationAndSmearingTool->applyCorrection(**electronsSC_itr)) Error("PassElectron()", "Cannot calibrate electron");

      if (objectFilter->PassElectron((*electronsSC_itr))){
        ScaleCount += 1;
        ScaleLep2 = ScaleLep1;
        charge2 = charge1;
        ScaleLep1 = (*electronsSC_itr)->p4();
     	charge1 = (*electronsSC_itr)->charge();

      }
      numEl++;
    }
    if (ScaleCount == 2) {
      if (charge1 * charge2 < 0){
        ScaleRef = ScaleLep1 + ScaleLep2;
        if (( ScaleRef.M() > Z_Min and ScaleRef.M() < Z_Max)){
          Syst->push_back(make_pair(TString(sysListItr->name()), ScaleRef));
        }
      }
    }
  }
  delete electrons_shallowCopy.first;
  delete electrons_shallowCopy.second;

 
  if (count != 2){
    Fail("2 leptons");
  }

  return true; 
}

// ---------------------------------------------------------
bool EventFilter::PassMuon(xAOD::TEvent *m_event)
{
  // ///////////////////
  // Get Muon collection
  // ///////////////////
  const xAOD::MuonContainer* muons = 0;
  if ( !m_event->retrieve( muons, "Muons" ).isSuccess() ){ 
    Error("execute()", "Failed to retrieve MuonCollection container. Exiting." );
    Fail("2 leptons");
    return false;
  }
  ConstDataVector<xAOD::MuonContainer> metMuons(SG::VIEW_ELEMENTS);

  pair< xAOD::MuonContainer*, xAOD::ShallowAuxContainer* > muons_shallowCopy = xAOD::shallowCopyContainer( *muons );
  xAOD::MuonContainer::iterator mu_itr = (muons_shallowCopy.first)->begin();
  xAOD::MuonContainer::iterator mu_end = (muons_shallowCopy.first)->end();
  xAOD::MuonContainer::const_iterator  muon_itr=muons->begin();

  // //////////////////////////////
  // Quick check to make sure
  // there's at least one 
  // lepton, check to make sure 
  // there's two good ones is later
  // //////////////////////////////
  if (mu_itr == mu_end){
    Fail("2 leptons");
    return false;
  }
  int charge1 = 0;
  int charge2 = 0;
  int count = 0;



  xAOD::MuonContainer* musCorrNom = muons_shallowCopy.first;
  xAOD::MuonContainer::iterator mu_it_last = musCorrNom->end();
  vector<CP::SystematicSet>::const_iterator sysListItr;
  for( xAOD::MuonContainer::iterator mu_it = musCorrNom->begin(); mu_it != mu_it_last; ++mu_it, ++muon_itr ){
    beforeCal = (*mu_it)->p4();
    if(!m_muonCalibrationAndSmearingTool->applyCorrection(**mu_it)) Error("PassMuon()", "Cannot calibrate muon");
    if (objectFilter->PassMuon((*mu_it))){
      metMuons.push_back((*muon_itr));
      count += 1;
      Lep2 = Lep1;
      charge2 = charge1;
      Lep1 = (*mu_it)->p4();
      //cout << (*mu_it)->primaryTrackParticle.p4().Pt() << endl;
      UncalLep2 = UncalLep1;
      UncalLep1 = beforeCal;
      // ////////////////////////////////////////////////
      // Muon_v1 sometimes fails to get the muons charge, 
      // instead of crashing for now I'm happy to say the 
      // charge was 0 since the event will failed the 
      // selectrion anyway
      // //////////////////////////////////////////////// 
      try {charge1 = (*mu_it)->charge();}
      catch (exception& e) {charge1 = 0;}
    }
  }



  assert(metMaker->rebuildMET("RefMuon",
                              xAOD::Type::Muon,
                              newMetContainer,
                              metMuons.asDataVector(),
                              metMap, MissingETBase::UsageHandler::PhysicsObject) // I'm not sure what should go here and I haven't managed to track down any documentation yet.  
                            );

  if (count == 2) {
    if (charge1 * charge2 < 0){
      Ref = Lep1 + Lep2;
        if (!( Ref.M() > Z_Min and Ref.M() < Z_Max)) Fail("Z Mass");
      }
    else Fail("Charge");
  }
  // ///////////////////////
  // Vary lepton systematics
  // ///////////////////////
  for (sysListItr = sysList.begin(); sysListItr != sysList.end(); ++sysListItr){
    int ScaleCount = 0;
    charge1 = 0;
    charge2 = 0;
    // //////////////////////////////////////////////////
    // Tell the calibration tool which variation to apply
    // ////////////////////////////////////////////////// 
    //if (!(sysListItr->name().find("EG")) != string::npos){
      if( m_muonCalibrationAndSmearingTool->applySystematicVariation( *sysListItr ) != CP::SystematicCode::Ok ) {
        Error("execute()", "Cannot configure muon calibration tool for systematic" );
        continue; // go to next systematic
      }
    //} 

    pair< xAOD::MuonContainer*, xAOD::ShallowAuxContainer* > muons_shallowCopy = xAOD::shallowCopyContainer( *muons );
    xAOD::MuonContainer::iterator muonSC_itr = (muons_shallowCopy.first)->begin();
    xAOD::MuonContainer::iterator muonSC_end = (muons_shallowCopy.first)->end();

    for( ; muonSC_itr != muonSC_end; ++muonSC_itr ) {
      if (((*muonSC_itr)->pt()/GeV < 1) or (fabs((*muonSC_itr)->eta())>2.4))  continue;
      if(!m_muonCalibrationAndSmearingTool->applyCorrection(**muonSC_itr)) Error("PassMuon()", "Cannot calibrate muon");
      if (objectFilter->PassMuon((*muonSC_itr))){
        ScaleCount += 1;
        ScaleLep2 = ScaleLep1;
        charge2 = charge1;
        ScaleLep1 = (*muonSC_itr)->p4();
        // ditto from above
        try {charge1 = (*muonSC_itr)->charge();}
        catch (exception& e) {charge1 = 0;}
      }
    }
    delete muons_shallowCopy.first;
    delete muons_shallowCopy.second;
    if (ScaleCount == 2) {
      if (charge1 * charge2  < 0){
        ScaleRef = ScaleLep1 + ScaleLep2;
        if (( ScaleRef.M() > Z_Min and ScaleRef.M() < Z_Max)){
          Syst->push_back(make_pair(TString(sysListItr->name()), ScaleRef));
        }
      }
    }
  }

  delete muons_shallowCopy.first;
  delete muons_shallowCopy.second;
  if (count != 2){
    Fail("2 leptons");
    return false;
  }
  return true;
}



// ---------------------------------------------------------
bool EventFilter::AddCut(string Cut)
{
  Cuts.push_back(Cut);
  CutMask.push_back(true);
  BalanceCutMask.push_back(true);
  return true;
}

// ---------------------------------------------------------
vector<double> EventFilter::GetSettings(TEnv *settings, string cut, double nom)
{
  vector<double> cuts;
  cuts.push_back(settings->GetValue( (cut+"_tight").c_str(), nom));
  cuts.push_back(settings->GetValue( (cut+"_nominal").c_str(), nom));
  cuts.push_back(settings->GetValue( (cut+"_loose").c_str(), nom)); 
  return cuts; 
}

// --------------------------------------------------------
void EventFilter::Fail(int cut)
{
  CutMask[cut] = false;
  BalanceCutMask[cut] = false;
  if (MPF_FirstFailed == FindCode("All Passed")){
    MPF_FirstFailed = cut;
  }
  if (Bal_FirstFailed == FindCode("All Passed")){
    Bal_FirstFailed = cut;
  }
}

// --------------------------------------------------------
void EventFilter::Mode(string mode)
{
  cout << endl;
  cout << "******************************" << endl;
  cout << "Running in " << mode << endl;
  cout << "******************************" <<  endl;
  cout << endl;
}

// -------------------------------------------------------
EventFilter::~EventFilter()
{
  objectFilter->Clean();
  delete objectFilter;
  delete m_jetCleaning;
  // //////////////
  // Removing tools
  // //////////////
  if (! m_JetCalibrationTool->finalize().isSuccess() ){
    Error("Clean()", "Failed to properly finalize the JetCalibrationTool Tool. Exiting." );
  }
  delete m_JetCalibrationTool;
  m_JetCalibrationTool = 0;
  if (! m_GSCJetCalibrationTool->finalize().isSuccess() ){
    Error("Clean()", "Failed to properly finalize the JetCalibrationTool Tool. Exiting." );
  }
  delete m_GSCJetCalibrationTool;
  m_GSCJetCalibrationTool = 0;

 
  delete  m_JVTtool;
  m_JVTtool=0;

  delete metMaker;
  delete metSystTool;

  if (not isPhoton){
    if(m_muonCalibrationAndSmearingTool){
      delete m_muonCalibrationAndSmearingTool;
      m_muonCalibrationAndSmearingTool = 0;
    }
  }
  if(m_grl){
    delete m_grl;
    m_grl = 0;
  }

/*  if (! m_EgammaCalibrationAndSmearingTool->finalize().isSuccess() ){
    Error("Clean()", "Failed to properly finalize the EgammaCalibrationAndSmearingTool. Exiting." );
  }    
*/
  delete m_EgammaCalibrationAndSmearingTool;
  m_EgammaCalibrationAndSmearingTool = 0;

  // ////////////////////
  // Clean object storage
  // ////////////////////
  isolatedJets->clear();
  delete isolatedJets;
  delete isolatedJetsAux;
  isolatedJetsLooseJVT->clear();
  delete isolatedJetsLooseJVT;
  delete isolatedJetsLooseJVTAux;
  isolatedJetsTightJVT->clear();
  delete isolatedJetsTightJVT;
  delete isolatedJetsTightJVTAux;
  goodCalibJets->clear();
  delete goodCalibJets;
  delete goodCalibJetsAux;

  if (photons_shallowCopy.first){
    photons_shallowCopy.first->clear();
    delete photons_shallowCopy.first;
    delete photons_shallowCopy.second;
  }

  Syst->clear();
  delete Syst;
  MPFSyst->clear();
  delete MPFSyst;
  BalSyst->clear();
  delete BalSyst;
}
