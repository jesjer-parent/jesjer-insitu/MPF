#!/usr/bin/env python

# Basic imports
import time
import getopt
import sys,os
import subprocess

#################################################
#################################################
#
# I'm looking to make submitting job to the grid 
# less involved so I don't need to babysit 
# submitting all 9 groups of samples to the grid
# when I want to update numbers
#
#################################################
#################################################



###################################################
# First things first, your username is needed to
# submit to the grid, and a unique name to identify
# this submission will help to find it later
###################################################
cernUsername = 'ahorton'
runName      = 'Rings_Weights2'

#PFlowReal2
############################################
# List channels you wish to run over
# Options are 'Electron' 'Muon' and 'Photon'
############################################
#Channels = ['Photon'] 
Channels = [ 'Electron', 'Muon']

############################################
# List samples which you would like to run 
# over for each channel.  Options are 'data', 
# 'Pythia' and 'Sherpa'.  To see which tags
# are actually being run over look later
# in list little script. 
############################################
#Samples = ['data', 'Pythia'] #, 'Sherpa', 'SherpaQ']
Samples = ['Pythia'] #, 'Sherpa', 'SherpaQ']

###############################################
# List scales you wish to calculate response at
# Currently 'EM' and 'LC' are options
# To be added: 'PFlow'
###############################################
Scales = [ 'EM', 'LC'] #, 'PFlow'] 
#Scales = ['PFlow' ]

sampleNames = [ 
         ['Electron_data',     'data16_13TeV.*00*Main*JETM3*2880*/'],
         ['Electron_Pythia',   'mc15_13TeV.*PowhegPythia*Zee*JETM3*r7725*p2879/'], 
         ['Electron_PythiaLow','group.perf-jets.361106*lowpt*EXT0*/'],
         ['Electron_Sherpa',   'mc15_13TeV.**Sherpa*NNPDF30NNLO*Zee_Pt*JETM3*r7725**p2879*/'],
         ['Electron_Madgraph', 'mc15_13TeV.*adGr*Zee*JETM3*r77*p2646*/'],
         ['Muon_data',         'data16_13TeV.*00*Main*JETM3*2880*/'],
         ['Muon_PythiaLow',    'group.perf-jets.361107*lowpt*EXT0*/'],
         ['Muon_Pythia',       'mc15_13TeV.*PowhegPythia*Zmumu*JETM3*r7725*p2879*/'],
         ['Muon_Sherpa',       'mc15_13TeV.*Sherpa*NNPDF30NNLO*mumu_Pt*JETM3*r7725*p2879*/'], 
         ['Muon_Madgraph',     'mc15_13TeV.*adGr*Zmumu*JETM3*r77*p2613*/'],
         ['Photon_data',       'data16_13TeV.*00*Main*JETM4*p2840*/'],
         ['Photon_Pythia',     'mc15_13TeV.**Pythia*amma*JETM4*7725*/'], 
         ['Photon_Sherpa',     'mc15_13TeV.*erpa*Photon*JETM4*7725*/'],
         ['Photon_SherpaQ',    'mc15_13TeV.**Sherpa_CT10_SinglePhotonPt15_35_QCUT5*JETM4*7725*/']
         ]


for channel in Channels:
  for sample in Samples:
    for scale in Scales:

      selectionFile = "Selections/"+channel
      jets          = "AntiKt4"+scale+"TopoJets"
      if scale == 'EM':
        MET         = 'MET_EMTopo'
      elif scale == 'LC':
        MET 	    = 'MET_LocHadTopo'
      elif scale == "PFlow":
        jets = "AntiKt4EMPFlowJets" 
        MET         = 'MET_EMTopo' #PFlow'

      samplename = channel+"_"+sample
      sampleSearch=''
      for n in sampleNames:
        if n[0] == samplename: sampleSearch = n[1]
      outputName = 'user.'+cernUsername+'.'+sample+'_'+scale+'_'+channel+'_'+runName+'.%in:name[2]%.%in:name[5]%.%in:name[6]%'
#      outputName = 'user.'+cernUsername+'.'+sample+'_'+scale+'_'+channel+'_'+runName+'.%in:name[3]%.%in:name[6]%.%in:name[7]%'

      isMC = 'true'
      if (sample=='data' or sample=='Data'): isMC='false' 
      
      OutDirNameMET = "GridOutdir" 
      print selectionFile, jets, MET, sampleSearch, outputName


      subprocess.call('cp share/configFile_template share/configFile',shell=True)
      command = "sed -i 's|_SELECTIONFILE_|"+selectionFile+"|g' share/configFile"
      subprocess.call(command,shell=True)
      command = "sed -i 's|_JETS_|"+jets+"|g' share/configFile"
      subprocess.call(command,shell=True)
      command = "sed -i 's|_MET_|"+MET+"|g' share/configFile"
      subprocess.call(command,shell=True)
      command = "sed -i 's|_OUTDIRNAME_|"+OutDirNameMET+"|g' share/configFile"
      subprocess.call(command,shell=True)
      command = "sed -i 's|_ISMC_|"+isMC+"|g' share/configFile"
      subprocess.call(command,shell=True)
      command = "sed -i 's|_SAMPLESEARCH_|"+sampleSearch+"|g' share/configFile"
      subprocess.call(command,shell=True)
      command = "sed -i 's|_OUTPUTNAME_|"+outputName+"|g' share/configFile"
      subprocess.call(command,shell=True)
      command = "RunGrid --config share/configFile"
      subprocess.call(command,shell=True)


