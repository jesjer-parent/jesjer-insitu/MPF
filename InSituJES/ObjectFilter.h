#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"

#ifndef OBJECTFILTER_H
#define OBJECTFILTER_H


#ifndef __CINT__
#include "xAODJet/Jet.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "IsolationSelection/IsolationSelectionTool.h"
#include "IsolationCorrections/IsolationCorrectionTool.h"
#include "MuonSelectorTools/MuonSelectionTool.h"
#include "ElectronPhotonShowerShapeFudgeTool/ElectronPhotonShowerShapeFudgeTool.h"
#include "ElectronPhotonSelectorTools/AsgPhotonIsEMSelector.h"
#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"
#endif /* __CINT __ */

#include "TEnv.h"
#include "TLorentzVector.h"
#include <vector>
#include <string> 
#include <math.h>


//class JetCleaningTool;
class egammaEnergyCorrectionTool;

class ObjectFilter
{

   protected:

   public:
      // Constructor
      ObjectFilter();

      // Destructor
      ~ObjectFilter();

      std::string Mode;
      
      // ///////////
      // Photon cuts
      // ///////////
      double MinPt_Photon;
      double MaxEta_Photon;
      std::string PhQual;
      // Isolation energy and isLoose saved for purity stupies;
      double IsolE;
      double isTight;
      double isIsol;

      bool PassNomID;
      bool PassNomIsol;
      bool isMC;
 
      // /////////////
      // Electrom cuts 
      // /////////////
      double MinPt_Electron;
      std::string ElQual;

      // /////////
      // Muon cuts
      // /////////
      double MinPt_Muon;
      double MaxEta_Muon;
      double RelIsolation_Muon;

      int FirstFailed;

      #ifndef __CINT__
      ////////
      // Tools
      ////////
      CP::IsolationSelectionTool *m_EgammaIsolationSelectionTool=0;
      CP::IsolationCorrectionTool* isoCorr_tool; //!
      CP::MuonSelectionTool *m_MuonSelectorTool=0;
      ElectronPhotonShowerShapeFudgeTool* m_fudgeMCTool=0;
      AsgPhotonIsEMSelector* m_photonTightIsEMSelector=0;
      AsgPhotonIsEMSelector* m_photonLooseIsEMSelector=0;
      AsgPhotonIsEMSelector* m_photonLoosePIsEMSelector=0;
      AsgElectronLikelihoodTool* m_LHTool; //!


      // /////////
      // Functions
      // /////////
      bool PassPhoton(xAOD::Photon *photon, std::vector<int> *failed, bool isNom);
      bool PassElectron(xAOD::Electron *electron);
      bool PassMuon(const xAOD::Muon *muon);
      bool PassJet(const xAOD::Jet *jet);
      #endif

      // /////////
      // Functions
      // /////////
      bool Init(TString Config, std::vector<std::string> *cuts, bool isMC);
      int GetFirstFailed() {return FirstFailed;};
      void Clean();
      bool LoosePrime(unsigned int val);

      // ///////////////////////////////
      // A few quick functions to access
      // information about the cutflow
      // ///////////////////////////////
      std::vector<std::string> *Cuts;
      int FindCode(std::string String){ if ((find(Cuts->begin(), Cuts->end(), String) - Cuts->begin()) == 24) std::cout << "problem " << String << std::endl; return find(Cuts->begin(), Cuts->end(), String) - Cuts->begin();};
      int NumCuts(){ return sizeof(*Cuts);};
      std::string GetCuts(int cut){return (*Cuts)[cut];};

      double GeV = 1000.;
};
#endif
