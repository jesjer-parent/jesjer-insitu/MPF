#ifndef EVENTFILTER_H
#define EVENTFILTER_H
#include "xAODMissingET/MissingETAuxContainer.h"
#include "xAODMissingET/MissingETAssociationMap.h"
#include "xAODMissingET/MissingETContainer.h"


#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "TEnv.h"
#include "TLorentzVector.h"
#include <vector>
#include <string>

#ifndef __CINT__
#include "JetCalibTools/JetCalibrationTool.h"
#include "JetMomentTools/JetVertexTaggerTool.h"
#include "ElectronPhotonFourMomentumCorrection/EgammaCalibrationAndSmearingTool.h"
#include "MuonMomentumCorrections/MuonCalibrationAndSmearingTool.h"
#include <InSituJES/ObjectFilter.h>
//#include <PileupReweighting/TPileupReweighting.h>
//#include "PileupReweighting/PileupReweightingTool.h"

#include "xAODJet/JetAuxContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODEgamma/ElectronAuxContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/PhotonAuxContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODTruth/xAODTruthHelpers.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonAuxContainer.h"
#include "xAODCore/ShallowCopy.h"
#include "METUtilities/METSystematicsTool.h"
#include "METUtilities/METMaker.h"

#include "PATInterfaces/SystematicVariation.h"
#include "PATInterfaces/SystematicRegistry.h"
#include "PATInterfaces/SystematicCode.h"
#endif /* __CINT __ */

class GoodRunsListSelectionTool;
class JetCleaningTool;

namespace xAOD{
  class Jet_v1;
  typedef Jet_v1 Jet;
  class Photon_v1;
  typedef Photon_v1 Photon;
}

using namespace std;
using namespace xAOD;

class EventFilter
{

   protected:

   public:

      // Constructor
      EventFilter(TString configFile); 

      // Destructor
      ~EventFilter();

      enum Variation {tight,nominal,loose};

      #ifndef __CINT__
      //  Tools
      GoodRunsListSelectionTool* m_grl=0; //!
      CP::EgammaCalibrationAndSmearingTool *m_EgammaCalibrationAndSmearingTool=0; 
      CP::MuonCalibrationAndSmearingTool *m_muonCalibrationAndSmearingTool=0; 

      met::METSystematicsTool *metSystTool=0;
      met::METMaker *metMaker=0;
     const xAOD::MissingETAssociationMap* metMap = nullptr;
      xAOD::MissingETContainer*    newMetContainer=0;


      JetCalibrationTool *m_JetCalibrationTool=0;
      JetCalibrationTool *m_GSCJetCalibrationTool=0;
      JetCleaningTool *m_jetCleaning=0;
      JetVertexTaggerTool *m_JVTtool=0;

      // Containers
      xAOD::JetContainer* goodCalibJets = 0;
      xAOD::JetAuxContainer* goodCalibJetsAux = 0;
      xAOD::JetContainer* isolatedJets = 0;
      xAOD::JetAuxContainer* isolatedJetsAux = 0;
      xAOD::JetContainer* isolatedJetsLooseJVT = 0;
      xAOD::JetAuxContainer* isolatedJetsLooseJVTAux = 0;
      xAOD::JetContainer* isolatedJetsTightJVT = 0;
      xAOD::JetAuxContainer* isolatedJetsTightJVTAux = 0;
      xAOD::Jet* newJet = 0;
      pair< xAOD::JetContainer*, xAOD::ShallowAuxContainer* > CopyJets;

//      pair< xAOD::ElectronContainer*, xAOD::ElectronAuxContainer* > electrons_shallowCopy;
      xAOD::Electron* newElectron = 0;

      pair< xAOD::MuonContainer*, xAOD::MuonAuxContainer* > muons_shallowCopy;
      xAOD::Muon* newMuon = 0;

      pair< xAOD::PhotonContainer*, xAOD::PhotonAuxContainer* > photons_shallowCopy;
      xAOD::Photon* newPhoton = 0;



      // Extra defs
      ObjectFilter *objectFilter;
      const xAOD::EventInfo* eventInfo = 0;
      const xAOD::VertexContainer* pVertices = 0;
      const xAOD::Vertex* pVertex = 0;

      vector<CP::SystematicSet> sysList;
      vector<CP::SystematicSet> GetsysList(){return sysList;};
      #endif

      vector<string> Cuts; //!
      vector<bool> CutMask;
      vector<bool> BalanceCutMask;

      // /////////
      // Functions
      // ///////// 
      bool PassVJet(xAOD::TEvent *m_event); 
      bool EventLevel(xAOD::TEvent *m_event); 
      bool Init(TString configFile); 
      bool CheckJets(xAOD::TEvent *m_event);
      bool PassPhoton(xAOD::TEvent *m_event); 
      bool PassElectron(xAOD::TEvent *m_event); 
      bool PassMuon(xAOD::TEvent *m_event); 
      bool FillMask(string);
      void InitTrig();
      void Fail(string cut){Fail(FindCode(cut));};
      void Fail(int cut);
      void Mode(string);

      // ///////
      // Getters
      // ///////
      vector<double> GetSettings(TEnv *settings, string cut, double nom);
      vector<pair<TString, TLorentzVector>>* GetMPFSyst(){return MPFSyst;};
      vector<pair<TString, TLorentzVector>>* GetBalSyst(){return BalSyst;};
      vector<bool> GetMask() {return CutMask;};
      vector<bool> GetBalanceMask() {return BalanceCutMask;};
      string GetMode() {return mode;};
      int GetMPFFirstFailed() {return MPF_FirstFailed;}; 
      int GetBalFirstFailed() {return Bal_FirstFailed;}; 
      bool GetIsMC() {return isMC;};

      // ///////////////////////////////
      // A few quick functions to access 
      // information about the cutflow.
      // ///////////////////////////////
      int FindCode(string String){ return find(Cuts.begin(), Cuts.end(), String) - Cuts.begin();};
      int NumCuts(){ return Cuts.size();};
      string GetCuts(int cut){return Cuts[cut];}; 
      bool AddCut(string);

      // ////////
      // Variable
      // ////////
      bool isMC;
      bool isElectron;
      bool isPhoton;
      bool isMuon;
      bool doGRL;
      string mode; 
      bool treatNegjvAsPileup; //!

      bool doSystsMPF;
      bool doSystsBal;
      int numPseudoExp;

      string grl;

      unsigned int iVertex;
      int NPV;

      TLorentzVector Lep1;
      TLorentzVector Lep2;
      TLorentzVector ScaleLep1;
      TLorentzVector ScaleLep2;
      TLorentzVector ScaleRef;
      TLorentzVector Ref;
      TLorentzVector UncalLep1;
      TLorentzVector UncalLep2;
      TLorentzVector beforeCal;
      //TLorentzVector Test1;
      //TLorentzVector Test2;
      TLorentzVector leadingJet;
      TLorentzVector leadingJetConst;
      TLorentzVector leadingJetPileupCorr;
      TLorentzVector subleadingJet;
      
      // /////////////////////////////
      // vectors hold systs name and  
      // varied ref for all variations 
      // which pass the selection     
      // /////////////////////////////
      vector<pair<TString, TLorentzVector>> *MPFSyst=0;
      vector<pair<TString, TLorentzVector>> *BalSyst=0;
      vector<pair<TString, TLorentzVector>> *Syst=0;

      string dataRootFilename;
      string MCRootFileName;
      string JetCollection;
      string METCollection;
      string name;
      
      double jvtCut; //!
      double jvtCutLoose; //!
      double jvtCutTight; //!
      double maxLeadingJetEta; //!
      double eta_jvtCut; //!
      double minPt_jvtCut; //!
      double leadingJet_MinPt; //!
      double ph_overlapRemoval; //!
      double lep_overlapRemoval; //!
      double Z_Min; //!
      double Z_Max; //!
//      vector<pair<string, double>> triggerBins;
      vector<pair<string, pair<double, double>>> triggerBins;
      vector<pair<string, pair<double, double>>> GetTriggerBins() {return triggerBins;};
//      vector<pair<string, double>> GetTriggerBins() {return triggerBins;};
      vector<double> min_delta_phi; //!
      vector<double> j2_maxRelPt; //!
      vector<double> j2_maxAbsPt; //!
      vector<double> min_delta_phi_balance; //!
      vector<double> j2_maxRelPt_balance; //!
      vector<double> j2_maxAbsPt_balance; //!

      double RelSubleadingJetPt; //!
      double subleadingJetEta; //!
      double dPhi; //!
      double corrX, corrY; //!
      double METUtilx, METUtily;
      double leadingJetGSC;
      double leadingJetJES;
      bool HardScat;
      bool passTight;
      bool isPFlow;
      bool doTracks;
      double isTight;
      double TrueJetPt;
      double TrueMF;
      double deltaRnearestJet;

      double leadingPhoton_MinPt; //!
      int MPF_FirstFailed; //!
      int Bal_FirstFailed; //!

      double GeV = 1000.;

};

#endif
