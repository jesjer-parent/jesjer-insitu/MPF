#ifndef InSituJES_JESAnalysis_H
#define InSituJES_JESAnalysis_H
// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "InSituJES/EventFilter.h"
//#include "InSituJES/ObjectFilter.h"
#include <PileupReweighting/TPileupReweighting.h>
#include "PileupReweighting/PileupReweightingTool.h"
#include "AsgAnalysisInterfaces/IPileupReweightingTool.h"
#include "AsgTools/AnaToolHandle.h"
#include <EventLoop/Algorithm.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>
#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "PFlowUtils/WeightPFOTool.h"


#include "BootstrapGenerator/BootstrapGenerator.h"
#include "BootstrapGenerator/TH2DBootstrap.h"

using namespace std;

namespace TrigConf {
   class xAODConfigTool;
}
namespace Trig {
   class TrigDecisionTool;
}

// Pileup reweighting
namespace CP {
   class PileupReweightingTool;
}

typedef map<string,TH2DBootstrap*> BootstrapMap;
class JESAnalysis : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.

  // Event counters;
  int m_eventCounter; //!
  int m_numCleanEvents; //!
  int n_ptBins; //!

  bool doRadiationStudies   = true; //!
  bool doPileupStudies      = true; //!
  bool doJetEtaStudies      = true; //!
  bool doJetDistanceStudies = true; //!

  bool doRingStudies = false; //!
  double furthestRing = 2; //!
  const int numRings=40; //!

  CP::WeightPFOTool *WeightTool=0; //!
  
  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:

  // Histogrtams
  map<string,TH1*> m_TH1;  //!
  map<string,TH2*> m_TH2; //!
  map<string,TH3*> m_TH3; //!

  UInt_t n_gProbePtBins; //!
  vector<double> ptBins; //!

  #ifndef __CINT__
  CP::PileupReweightingTool* m_pileupTool;//!
  double pileupWeight; //!
  string dataRootFilename; //!
  string MCRootFileName; //!
  #endif

  EventFilter *theFilter; //!
  vector<string> systNames; //!

  // this is a standard constructor
  JESAnalysis ();
  ~JESAnalysis();
  string Config="boo";

  void SetConfig() {
    const char* temp = "$ROOTCOREBIN/data/InSituJES/configFile";
    Config = gSystem->ExpandPathName (temp);
  };

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput ();
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // A few extra functions
  vector<string> vectorizeStr (string bins);
  bool Pass(vector<bool> Mask, string cut);

  // Temporary functions for preliminary look at other activity studies
  double PartialResponseFit(double RefEnergy);
  double FullResponseFit(double RefEnergy);
  double CalcOA(double Resp, TLorentzVector jet, TLorentzVector Ref, bool dokTerm);


  // Useful definitions
  double GeV = 1000;
  double mb = 0.001;
  double ub = 0.001*mb;
  double nb = 0.001*ub;
  double pb = 0.001*nb;
  double fb = 0.001*pb;
  double ab = 0.001*fb;
  double zb = 0.001*ab;
 
  // this is needed to distribute the algorithm to the workers
  ClassDef(JESAnalysis, 1);

  xAOD::TEvent *m_event;  //!


  BootstrapGenerator *m_generator; //!
  BootstrapMap m_data; //!
  private:
  /// The trigger configuration tool
  TrigConf::xAODConfigTool* m_confTool; //!
  /// The trigger decision tool
  Trig::TrigDecisionTool* m_tdt; //!
};
#endif
