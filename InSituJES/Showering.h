#ifndef SHOWERING_H
#define SHOWERING_H

#include <EventLoop/Algorithm.h>

#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>
#include "xAODCalCellInfo/CalCellInfoContainer.h"
#include "xAODCalCellInfo/CalCellInfo.h"

// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"

#include "fastjet/ClusterSequence.hh"
#include <iostream>
#include "xAODRootAccess/tools/Message.h"
#include "xAODMissingET/MissingETContainer.h"

// EDM includes:
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"
//#include "xAODCalCellInfo/CalCellInfoContainer.h"
//#include "xAODCalCellInfo/CalCellInfo.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthVertex.h"
#include "xAODCaloEvent/CaloCluster.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODPFlow/PFOContainer.h"
#include "PFlowUtils/WeightPFOTool.h"



using namespace fastjet;

using namespace std;
using namespace xAOD;
class Showering 
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
  public:
  // float cutValue;

  double GeV=1000.0;

  // Histogrtams
  map<string,TH1*> m_TH1;  //!
  map<string,TH2*> m_TH2; //!
  map<string,TH3*> m_TH3; //!
  int count=0;


  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:
  // Tree *myTree; //!
  // TH1 *myHist; //!



  // this is a standard constructor
  Showering ();
  ~Showering();

  bool GetShowering(xAOD::TEvent*);

  bool CompareCells(CalCellInfo* i, CalCellInfo* j)
  {
    return i->cellEMRecoEnergy() < j->cellEMRecoEnergy();
  }

  struct sortCells
  {
   //  overload () operator
   bool operator() (const CalCellInfo* i, const CalCellInfo* j)
   {
      return i->cellEMRecoEnergy()< j->cellEMRecoEnergy();
    }
  };



};

#endif
